<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class BackgroundType extends HxEnum {
    
	/**
     * @return BackgroundType
     */
	public static function __callStatic($name, $args)
    {
        switch($name){
			case "BG_GARDEN" : return BackgroundType::BG_GARDEN();
			case "BG_ATTIC"  : return BackgroundType::BG_ATTIC ();
			case "BG_SEWER"  : return BackgroundType::BG_SEWER ();
			case "BG_ROAD"   : return BackgroundType::BG_ROAD  ();
			case "BG_WOOD"   : return BackgroundType::BG_WOOD  ();
		}
    }
	
    /**
     * @return BackgroundType
     */
    static public function BG_GARDEN () { return new BackgroundType("BG_GARDEN", 0, []); }
	
    /**
     * @return BackgroundType
     */
    static public function BG_ATTIC  () { return new BackgroundType("BG_ATTIC" , 1, []); }
	
    /**
     * @return BackgroundType
     */
    static public function BG_SEWER  () { return new BackgroundType("BG_SEWER" , 2, []); }
	
    /**
     * @return BackgroundType
     */
    static public function BG_ROAD   () { return new BackgroundType("BG_ROAD"  , 3, []); }
	
    /**
     * @return BackgroundType
     */
    static public function BG_WOOD   () { return new BackgroundType("BG_WOOD"  , 4, []); }

    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
            0 => "BG_GARDEN",
            1 => "BG_ATTIC" ,
            2 => "BG_SEWER" ,
            3 => "BG_ROAD"  ,
            4 => "BG_WOOD"  ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            "BG_GARDEN" => 0,
            "BG_ATTIC"  => 0,
            "BG_SEWER"  => 0,
            "BG_ROAD"   => 0,
            "BG_WOOD"   => 0,
        ];
    }
}

Boot::registerClass(BackgroundType::class, 'BackgroundType');