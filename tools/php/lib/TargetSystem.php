<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class TargetSystem extends HxEnum {
    
	/**
     * @return TargetSystem
     */
	public static function __callStatic($name, $args)
    {
        switch($name){
			case '0QCw\x01'      : return TargetSystem::__CU0();
			case 'rf_U\x03'      : return TargetSystem::__CU1();
			case 'f`\x16\x02' : return TargetSystem::__CU2();
			case '%i\x1e2\x01'   : return TargetSystem::__CU3();
		}
    }
	
    /**
     * @return TargetSystem
     */
    static public function __CU0 () { return new TargetSystem('0QCw\x01'     , 0, []); }
	
    /**
     * @return TargetSystem
     */
    static public function __CU1 () { return new TargetSystem('rf_U\x03'     , 1, []); }
	
    /**
     * @return TargetSystem
     */
    static public function __CU2 () { return new TargetSystem('f`\x16\x02', 2, []); }
	
    /**
     * @return TargetSystem
     */
    static public function __CU3 () { return new TargetSystem('%i\x1e2\x01'  , 3, []); }

    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
            0 => '0QCw\x01'     ,
            1 => 'rf_U\x03'     ,
            2 => 'f`\x16\x02',
            3 => '%i\x1e2\x01'  ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            '0QCw\x01'      => 0,
            'rf_U\x03'      => 0,
            'f`\x16\x02' => 0,
            '%i\x1e2\x01'   => 0,
        ];
    }
}

Boot::registerClass(TargetSystem::class, 'TargetSystem');