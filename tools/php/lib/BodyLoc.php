<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class BodyLoc extends HxEnum {
	
	/**
     * @return BodyLoc
     */
	static public function HEAD        () { return new BodyLoc("HEAD",        0, []); }

	/**
     * @return BodyLoc
     */
	static public function TORSO_LEFT  () { return new BodyLoc("TORSO_LEFT",  1, []); }

	/**
     * @return BodyLoc
     */
	static public function TORSO_RIGHT () { return new BodyLoc("TORSO_RIGHT", 2, []); }

	/**
     * @return BodyLoc
     */
	static public function STOMACH     () { return new BodyLoc("STOMACH",     3, []); }

	/**
     * @return BodyLoc
     */
	static public function ARM_LEFT    () { return new BodyLoc("ARM_LEFT",    4, []); }

	/**
     * @return BodyLoc
     */
	static public function ARM_RIGHT   () { return new BodyLoc("ARM_RIGHT",   5, []); }

	/**
     * @return BodyLoc
     */
	static public function LEG_LEFT    () { return new BodyLoc("LEG_LEFT",    6, []); }

	/**
     * @return BodyLoc
     */
	static public function LEG_RIGHT   () { return new BodyLoc("LEG_RIGHT",   7, []); }


	/**
	* Returns array of (constructorIndex => constructorName)
	*
	* @return string[]
	*/
	static public function __hx__list () {
		return [
			0 => "HEAD",       
			1 => "TORSO_LEFT", 
			2 => "TORSO_RIGHT",
			3 => "STOMACH",    
			4 => "ARM_LEFT",   
			5 => "ARM_RIGHT",  
			6 => "LEG_LEFT",   
			7 => "LEG_RIGHT",  
		];
	}

	/**
	* Returns array of (constructorName => parametersCount)
	*
	* @return int[]
	*/
	static public function __hx__paramsCount () {
		return [
			"HEAD"         => 0,
			"TORSO_LEFT"   => 0,
			"TORSO_RIGHT"  => 0,
			"STOMACH"      => 0,
			"ARM_LEFT"     => 0,
			"ARM_RIGHT"    => 0,
			"LEG_LEFT"     => 0,
			"LEG_RIGHT"    => 0,
		];
	}
}

Boot::registerClass(BodyLoc::class, 'BodyLoc');