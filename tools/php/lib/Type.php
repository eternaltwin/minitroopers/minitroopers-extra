<?php
/**
 */

use \php\Boot;
use \php\_Boot\HxClass;
use \haxe\Exception as HaxeException;
use \php\_Boot\HxClosure;
use \php\_Boot\HxEnum;

/**
 * The Haxe Reflection API allows retrieval of type information at runtime.
 * This class complements the more lightweight Reflect class, with a focus on
 * class and enum instances.
 * @see https://haxe.org/manual/types.html
 * @see https://haxe.org/manual/std-reflection.html
 */
class Type {
	/**
	 * Creates an instance of class `cl`.
	 * This function guarantees that the class constructor is not called.
	 * If `cl` is null, the result is unspecified.
	 * 
	 * @param Class $cl
	 * 
	 * @return mixed
	 */
	public static function createEmptyInstance ($cl) {
		#/usr/share/haxe/std/php/_std/Type.hx:135: lines 135-136
		if (Boot::getClass('String') === $cl) {
			#/usr/share/haxe/std/php/_std/Type.hx:136: characters 4-18
			return "";
		}
		#/usr/share/haxe/std/php/_std/Type.hx:137: lines 137-138
		if (Boot::getClass(\Array_hx::class) === $cl) {
			#/usr/share/haxe/std/php/_std/Type.hx:138: characters 4-18
			return new \Array_hx();
		}
		#/usr/share/haxe/std/php/_std/Type.hx:140: characters 3-68
		$reflection = new \ReflectionClass($cl->phpClassName);
		#/usr/share/haxe/std/php/_std/Type.hx:141: characters 3-52
		return $reflection->newInstanceWithoutConstructor();
	}

	/**
	 * Creates an instance of enum `e` by calling its constructor `constr` with
	 * arguments `params`.
	 * If `e` or `constr` is null, or if enum `e` has no constructor named
	 * `constr`, or if the number of elements in `params` does not match the
	 * expected number of constructor arguments, or if any argument has an
	 * invalid type, the result is unspecified.
	 * 
	 * @param Enum $e
	 * @param string $constr
	 * @param mixed[]|\Array_hx $params
	 * 
	 * @return mixed
	 */
	public static function createEnum ($e, $constr, $params = null) {
		#/usr/share/haxe/std/php/_std/Type.hx:145: lines 145-146
		if (($e === null) || ($constr === null)) {
			#/usr/share/haxe/std/php/_std/Type.hx:146: characters 4-15
			return null;
		}
		#/usr/share/haxe/std/php/_std/Type.hx:148: characters 3-43
		$phpName = $e->phpClassName;
		#/usr/share/haxe/std/php/_std/Type.hx:150: lines 150-152
		if (!in_array($constr, $phpName::__hx__list())) {
			#/usr/share/haxe/std/php/_std/Type.hx:151: characters 4-9
			throw HaxeException::thrown("No such constructor " . ($constr??'null'));
		}
		#/usr/share/haxe/std/php/_std/Type.hx:154: characters 3-92
		$paramsCounts = $phpName::__hx__paramsCount();
		#/usr/share/haxe/std/php/_std/Type.hx:155: lines 155-157
		if ((($params === null) && ($paramsCounts[$constr] !== 0)) || (($params !== null) && ($params->length !== $paramsCounts[$constr]))) {
			#/usr/share/haxe/std/php/_std/Type.hx:156: characters 4-9
			throw HaxeException::thrown("Provided parameters count does not match expected parameters count");
		}
		#/usr/share/haxe/std/php/_std/Type.hx:159: lines 159-164
		if ($params === null) {
			#/usr/share/haxe/std/php/_std/Type.hx:160: characters 4-45
			return $phpName::{$constr}();
		} else {
			#/usr/share/haxe/std/php/_std/Type.hx:162: characters 4-60
			$nativeArgs = $params->arr;
			#/usr/share/haxe/std/php/_std/Type.hx:163: characters 4-71
			return $phpName::{$constr}(...$nativeArgs);
		}
	}

	/**
	 * Returns the name of class `c`, including its path.
	 * If `c` is inside a package, the package structure is returned dot-
	 * separated, with another dot separating the class name:
	 * `pack1.pack2.(...).packN.ClassName`
	 * If `c` is a sub-type of a Haxe module, that module is not part of the
	 * package structure.
	 * If `c` has no package, the class name is returned.
	 * If `c` is null, the result is unspecified.
	 * The class name does not include any type parameters.
	 * 
	 * @param Class $c
	 * 
	 * @return string
	 */
	public static function getClassName ($c) {
		#/usr/share/haxe/std/php/_std/Type.hx:73: lines 73-74
		if ($c === null) {
			#/usr/share/haxe/std/php/_std/Type.hx:74: characters 4-15
			return null;
		}
		#/usr/share/haxe/std/php/_std/Type.hx:75: characters 3-34
		return Boot::getHaxeName($c);
	}

	/**
	 * Returns a list of the names of all constructors of enum `e`.
	 * The order of the constructor names in the returned Array is preserved
	 * from the original syntax.
	 * If `e` is null, the result is unspecified.
	 * 
	 * @param Enum $e
	 * 
	 * @return string[]|\Array_hx
	 */
	public static function getEnumConstructs ($e) {
		#/usr/share/haxe/std/php/_std/Type.hx:261: lines 261-262
		if ($e === null) {
			#/usr/share/haxe/std/php/_std/Type.hx:262: characters 4-15
			return null;
		}
		#/usr/share/haxe/std/php/_std/Type.hx:263: characters 3-66
		return \Array_hx::wrap($e->__hx__list());
	}

	/**
	 * Returns the name of enum `e`, including its path.
	 * If `e` is inside a package, the package structure is returned dot-
	 * separated, with another dot separating the enum name:
	 * `pack1.pack2.(...).packN.EnumName`
	 * If `e` is a sub-type of a Haxe module, that module is not part of the
	 * package structure.
	 * If `e` has no package, the enum name is returned.
	 * If `e` is null, the result is unspecified.
	 * The enum name does not include any type parameters.
	 * 
	 * @param Enum $e
	 * 
	 * @return string
	 */
	public static function getEnumName ($e) {
		#/usr/share/haxe/std/php/_std/Type.hx:79: characters 3-30
		return Type::getClassName($e);
	}

	/**
	 * Resolves a class by name.
	 * If `name` is the path of an existing class, that class is returned.
	 * Otherwise null is returned.
	 * If `name` is null or the path to a different type, the result is
	 * unspecified.
	 * The class name must not include any type parameters.
	 * 
	 * @param string $name
	 * 
	 * @return Class
	 */
	public static function resolveClass ($name) {
		#/usr/share/haxe/std/php/_std/Type.hx:83: lines 83-84
		if ($name === null) {
			#/usr/share/haxe/std/php/_std/Type.hx:84: characters 4-15
			return null;
		}
		#/usr/share/haxe/std/php/_std/Type.hx:85: lines 85-100
		if ($name === "Bool") {
			#/usr/share/haxe/std/php/_std/Type.hx:93: characters 5-21
			return Boot::getClass('Bool');
		} else if ($name === "Class") {
			#/usr/share/haxe/std/php/_std/Type.hx:97: characters 5-22
			return Boot::getClass('Class');
		} else if ($name === "Dynamic") {
			#/usr/share/haxe/std/php/_std/Type.hx:87: characters 5-24
			return Boot::getClass('Dynamic');
		} else if ($name === "Enum") {
			#/usr/share/haxe/std/php/_std/Type.hx:99: characters 5-21
			return Boot::getClass('Enum');
		} else if ($name === "Float") {
			#/usr/share/haxe/std/php/_std/Type.hx:91: characters 5-22
			return Boot::getClass('Float');
		} else if ($name === "Int") {
			#/usr/share/haxe/std/php/_std/Type.hx:89: characters 5-20
			return Boot::getClass('Int');
		} else if ($name === "String") {
			#/usr/share/haxe/std/php/_std/Type.hx:95: characters 5-18
			return Boot::getClass('String');
		}
		#/usr/share/haxe/std/php/_std/Type.hx:102: characters 3-40
		$phpClass = Boot::getPhpName($name);
		#/usr/share/haxe/std/php/_std/Type.hx:103: lines 103-104
		if (!class_exists($phpClass) && !interface_exists($phpClass)) {
			#/usr/share/haxe/std/php/_std/Type.hx:104: characters 4-15
			return null;
		}
		#/usr/share/haxe/std/php/_std/Type.hx:106: characters 3-41
		$hxClass = Boot::getClass($phpClass);
		#/usr/share/haxe/std/php/_std/Type.hx:108: characters 3-22
		return $hxClass;
	}

	/**
	 * Resolves an enum by name.
	 * If `name` is the path of an existing enum, that enum is returned.
	 * Otherwise null is returned.
	 * If `name` is null the result is unspecified.
	 * If `name` is the path to a different type, null is returned.
	 * The enum name must not include any type parameters.
	 * 
	 * @param string $name
	 * 
	 * @return Enum
	 */
	public static function resolveEnum ($name) {
		#/usr/share/haxe/std/php/_std/Type.hx:112: lines 112-113
		if ($name === null) {
			#/usr/share/haxe/std/php/_std/Type.hx:113: characters 4-15
			return null;
		}
		#/usr/share/haxe/std/php/_std/Type.hx:114: lines 114-115
		if ($name === "Bool") {
			#/usr/share/haxe/std/php/_std/Type.hx:115: characters 4-20
			return Boot::getClass('Bool');
		}
		#/usr/share/haxe/std/php/_std/Type.hx:117: characters 3-40
		$phpClass = Boot::getPhpName($name);
		#/usr/share/haxe/std/php/_std/Type.hx:118: lines 118-119
		if (!class_exists($phpClass)) {
			#/usr/share/haxe/std/php/_std/Type.hx:119: characters 4-15
			return null;
		}
		#/usr/share/haxe/std/php/_std/Type.hx:121: characters 3-41
		$hxClass = Boot::getClass($phpClass);
		#/usr/share/haxe/std/php/_std/Type.hx:123: characters 3-22
		return $hxClass;
	}

	/**
	 * Returns the runtime type of value `v`.
	 * The result corresponds to the type `v` has at runtime, which may vary
	 * per platform. Assumptions regarding this should be minimized to avoid
	 * surprises.
	 * 
	 * @param mixed $v
	 * 
	 * @return \ValueType
	 */
	public static function typeof ($v) {
		#/usr/share/haxe/std/php/_std/Type.hx:267: lines 267-268
		if ($v === null) {
			#/usr/share/haxe/std/php/_std/Type.hx:268: characters 4-16
			return \ValueType::TNull();
		}
		#/usr/share/haxe/std/php/_std/Type.hx:270: lines 270-282
		if (is_object($v)) {
			#/usr/share/haxe/std/php/_std/Type.hx:271: lines 271-272
			if (($v instanceof \Closure) || ($v instanceof HxClosure)) {
				#/usr/share/haxe/std/php/_std/Type.hx:272: characters 5-21
				return \ValueType::TFunction();
			}
			#/usr/share/haxe/std/php/_std/Type.hx:273: lines 273-274
			if (($v instanceof \StdClass)) {
				#/usr/share/haxe/std/php/_std/Type.hx:274: characters 5-19
				return \ValueType::TObject();
			}
			#/usr/share/haxe/std/php/_std/Type.hx:275: lines 275-276
			if (($v instanceof HxClass)) {
				#/usr/share/haxe/std/php/_std/Type.hx:276: characters 5-19
				return \ValueType::TObject();
			}
			#/usr/share/haxe/std/php/_std/Type.hx:278: characters 4-53
			$hxClass = Boot::getClass(get_class($v));
			#/usr/share/haxe/std/php/_std/Type.hx:279: lines 279-280
			if (($v instanceof HxEnum)) {
				#/usr/share/haxe/std/php/_std/Type.hx:280: characters 5-31
				return \ValueType::TEnum($hxClass);
			}
			#/usr/share/haxe/std/php/_std/Type.hx:281: characters 4-31
			return \ValueType::TClass($hxClass);
		}
		#/usr/share/haxe/std/php/_std/Type.hx:284: lines 284-285
		if (is_bool($v)) {
			#/usr/share/haxe/std/php/_std/Type.hx:285: characters 4-16
			return \ValueType::TBool();
		}
		#/usr/share/haxe/std/php/_std/Type.hx:286: lines 286-287
		if (is_int($v)) {
			#/usr/share/haxe/std/php/_std/Type.hx:287: characters 4-15
			return \ValueType::TInt();
		}
		#/usr/share/haxe/std/php/_std/Type.hx:288: lines 288-289
		if (is_float($v)) {
			#/usr/share/haxe/std/php/_std/Type.hx:289: characters 4-17
			return \ValueType::TFloat();
		}
		#/usr/share/haxe/std/php/_std/Type.hx:290: lines 290-291
		if (is_string($v)) {
			#/usr/share/haxe/std/php/_std/Type.hx:291: characters 4-25
			return \ValueType::TClass(Boot::getClass('String'));
		}
		#/usr/share/haxe/std/php/_std/Type.hx:293: characters 3-18
		return \ValueType::TUnknown();
	}
}

Boot::registerClass(Type::class, 'Type');
