<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class MoveSystem extends HxEnum {
    
	/**
     * @return MoveSystem
     */
	public static function __callStatic($name, $args)
    {
        switch($name){
			case '\n\x01O\x02' : return MoveSystem::__CU0();
			case '}C9g'           : return MoveSystem::__CU1();
			case 'J\x16$2'        : return MoveSystem::__CU2();
		}
    }
	
    /**
     * @return MoveSystem
     */
    static public function __CU0 () { return new MoveSystem('\n\x01O\x02', 0, []); }

    /**
     * @return MoveSystem
     */
    static public function __CU1 () { return new MoveSystem('}C9g'          , 1, []); }

    /**
     * @return MoveSystem
     */
    static public function __CU2 () { return new MoveSystem('J\x16$2'       , 2, []); }

    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
            0 => '\n\x01O\x02',
            1 => '}C9g'          ,
            2 => 'J\x16$2'       ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            '\n\x01O\x02' => 0,
            '}C9g'           => 0,
            'J\x16$2'        => 0,
        ];
    }
}

Boot::registerClass(MoveSystem::class, 'MoveSystem');