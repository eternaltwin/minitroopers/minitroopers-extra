<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class TargetType extends HxEnum {
    
	/**
     * @return TargetType
     */
	public static function __callStatic($name, $args)
    {
        switch($name){
			case '3YX,\x03' : return TargetType::__CU0();
			case 'RO)%'     : return TargetType::__CU1($args[0]);
		}
    }
	
    /**
     * @return TargetType
     */
    static public function __CU0 () { return new TargetType('3YX,\x03', 0, []); }
	
	/**
     * @param object $arg0
     * 
     * @return TargetType
     */
    static public function __CU1 ($arg0) {
        return new TargetType('RO)%', 0, [$arg0]);
    }

    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
            0 => '3YX,\x03',
            1 => 'RO)%'    ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            '3YX,\x03' => 0,
            'RO)%'     => 1,
        ];
    }
}

Boot::registerClass(TargetType::class, 'TargetType');