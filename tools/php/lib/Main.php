<?php
/**
 */

use \php\_Boot\HxAnon;
use \php\Boot;
use \haxe\Log;
use \haxe\Unserializer;
use \haxe\Serializer;

class Main {

	public static $dico = array( //Minitroopers
		'V\u001bE|'                 => "pref",
		'\fA2c'                     => "seed",
		'$PS\u0001'                 => "gfx",
		';\u0001\fZ'                => "mode",
		'An%6'                      => "type",
		'}&\u0016Y\u0001'           => "force",
		'\u00032\u001a4'            => "name",
		'7X\u0001'                  => "id",
		'\u000bR\u0010\u001b\u0002' => "__CWeapon",      // enum Weapon
		'\u0011!\u000ea\u0001'      => "reloadSystem",
		'\u0015S\u0017?\u0003'      => "targetType",
		'\u001a)\u0013=\u0001'      => "__CBody",        // enum BodyLoc
		'\u001dlQK\u0003'           => "moveSystem",
		')0\u0019i\u0003'           => "leftOver",
		';uXE'                      => "targetSystem",
		';\u001F\u000FJ'            => "army",
		'\u0005(\u000Bm\u0002'      => "action",
		'Vif0'                      => "ymax",
		'hW+a'                      => "xmax",
		'\u0017\u0006\t\u0001'      => "fac",
		'\u0010Gv\u0001'            => "add",
		'\ng\u0012;\u0001'          => "space",
		'\nz\nv'                    => "choices",
		'*M\u0001'                  => "bg",
		'a]e\u001b\u0002'           => "armies",
		'`1\u001f*\u0002'           => "troopers",
		'OPz\u0016\u0003'           => "faction",
		'K)\u0016\n\u0002'          => "__UP2"
	);
	public static $search  = array();
	public static $replace = array();
	
	/**
	 * @return void
	 */
	public function __construct () {
		Main::$search  = array_keys(Deobfu::$dico);
		Main::$replace = array_values(Deobfu::$dico);
	}

	/**
	 * @return void
	 */
	public static function main () {

	}

	public static function unserialize ($fv) {
		$unserializer = new Unserializer(urldecode($fv));
		return json_encode($unserializer->unserialize());
	}
	
	public static function deobfu($source){
		return str_replace(Main::$search, Main::$replace, $source);
	}

	
	
}

class _HxAnon_Main0 extends HxAnon {
	function __construct($fileName, $lineNumber, $className, $methodName) {
		$this->fileName = $fileName;
		$this->lineNumber = $lineNumber;
		$this->className = $className;
		$this->methodName = $methodName;
	}
}

Boot::registerClass(Main::class, 'Main');
