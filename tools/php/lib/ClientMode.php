<?php

use \php\Boot;
use \php\_Boot\HxEnum;

class ClientMode extends HxEnum {
    
	public static function __callStatic($name, $args)
    {
        switch($name){
			case "eU\\u\x01"         : return ClientMode::__CU0($args[0], $args[1]);
			case "F\fN\x02\x01"      : return ClientMode::__CU1($args[0]);
			case "\x1b\x11L\x1c\x02" : return ClientMode::__CU2($args[0], $args[1]);
			case "w{1\x01\x03"       : return ClientMode::__CU3($args[0]);
			case "R;,'\x01"          : return ClientMode::__CU4($args[0]);
			case "*!,\x05\x02"       : return ClientMode::__CU5($args[0], $args[1], $args[2]);
		}
    }
	
	/**
	 * 'eU\\u\x01'
     * @param object $arg0
     * @param int $arg1
     * 
     * @return ClientMode
     */
    static public function __CU0 ($arg0, $arg1) {
        return new ClientMode('eU\\u\x01', 0, [$arg0, $arg1]);
    }
	
	/**
     * @param object $arg0
     * 
     * @return ClientMode
     */
    static public function __CU1 ($arg0) {
        return new ClientMode('F\fN\x02\x01', 1, [$arg0]);
    }

    /**
     * @param object $arg0
     * @param int $arg1
     * 
     * @return ClientMode
     */
    static public function __CU2 ($arg0, $arg1) {
        return new ClientMode('\x1b\x11L\x1c\x02', 2, [$arg0, $arg1]);
    }
	
	/**
     * @param object $arg0
     * 
     * @return ClientMode
     */
    static public function __CU3 ($arg0) {
        return new ClientMode('w{1\x01\x03', 3, [$arg0]);
    }
	
	/**
     * @param object $arg0
     * 
     * @return ClientMode
     */
    static public function __CU4 ($arg0) {
        return new ClientMode('R;,\'\x01', 4, [$arg0]);
    }
	
	/**
     * @param object $arg0
     * @param int $arg1
     * @param int $arg2
     * 
     * @return ClientMode
     */
    static public function __CU5 ($arg0, $arg1, $arg2) {
        return new ClientMode('*!,\x05\x02', 5, [$arg0, $arg1, $arg2]);
    }
	
    /**
     * Returns array of (constructorIndex => constructorName)
     *
     * @return string[]
     */
    static public function __hx__list () {
        return [
            0 => "eU\\u\x01"        ,
            1 => "F\fN\x02\x01"     ,
            2 => "\x1b\x11L\x1c\x02",
            3 => "w{1\x01\x03"      ,
            4 => "R;,'\x01"         ,
            5 => "*!,\x05\x02"      ,
        ];
    }

    /**
     * Returns array of (constructorName => parametersCount)
     *
     * @return int[]
     */
    static public function __hx__paramsCount () {
        return [
            "eU\\u\x01"         => 2,
            "F\fN\x02\x01"      => 1,
            "\x1b\x11L\x1c\x02" => 2,
            "w{1\x01\x03"       => 1,
            "R;,'\x01"          => 1,
            "*!,\x05\x02"       => 3,
        ];
    }
}

Boot::registerClass(ClientMode::class, 'ClientMode');