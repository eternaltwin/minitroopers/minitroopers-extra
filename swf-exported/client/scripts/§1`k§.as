package
{
   import flash.utils.describeType;
   import flash.utils.getDefinitionByName;
   import flash.utils.getQualifiedClassName;
   import flash.utils.getQualifiedSuperclassName;
   import §pG?\x06\x03§.§\bRQ\f§;
   
   public class §1`k§
   {
       
      
      public function §1`k§()
      {
      }
      
      public static function §qaLj\x02§(param1:Object) : Class
      {
         var _loc2_:String = getQualifiedClassName(param1);
         if(_loc2_ == "null" || _loc2_ == "Object" || _loc2_ == "int" || _loc2_ == "Number" || _loc2_ == "Boolean")
         {
            return null;
         }
         if(param1.hasOwnProperty("prototype"))
         {
            return null;
         }
         var _loc3_:* = getDefinitionByName(_loc2_) as Class;
         if(_loc3_.__isenum)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §d`k\x11\x03§(param1:*) : Class
      {
         var _loc2_:String = getQualifiedClassName(param1);
         if(_loc2_ == "null" || _loc2_.substr(0,8) == "builtin.")
         {
            return null;
         }
         if(param1.hasOwnProperty("prototype"))
         {
            return null;
         }
         var _loc3_:* = getDefinitionByName(_loc2_) as Class;
         if(!_loc3_.__isenum)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §9A\x05\x1b\x02§(param1:Class) : Class
      {
         var _loc2_:String = getQualifiedSuperclassName(param1);
         if(_loc2_ == null || _loc2_ == "Object")
         {
            return null;
         }
         return getDefinitionByName(_loc2_) as Class;
      }
      
      public static function §XH\x11U\x03§(param1:Class) : String
      {
         if(param1 == null)
         {
            return null;
         }
         var _loc2_:String = getQualifiedClassName(param1);
         var _loc3_:String = _loc2_;
         if(_loc3_ == "int")
         {
            return "Int";
         }
         if(_loc3_ == "Number")
         {
            return "Float";
         }
         if(_loc3_ == "Boolean")
         {
            return "Bool";
         }
         return _loc2_.split("::").join(".");
      }
      
      public static function §N\x06Y(\x01§(param1:Class) : String
      {
         return §1`k§.§XH\x11U\x03§(param1);
      }
      
      public static function §9\x18YK§(param1:String) : Class
      {
         var _loc3_:* = null as Class;
         var _loc5_:* = null as String;
         try
         {
            _loc3_ = getDefinitionByName(param1) as Class;
            if(_loc3_.__isenum)
            {
               return null;
            }
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            _loc5_ = param1;
            if(_loc5_ == "Int")
            {
               return int;
            }
            if(_loc5_ == "Float")
            {
               return Number;
            }
            return null;
         }
         if(_loc3_ == null || _loc3_.__name__ == null)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §\x1f\x0f\x1dA\x01§(param1:String) : Class
      {
         var _loc3_:* = null;
         try
         {
            _loc3_ = getDefinitionByName(param1);
            if(!_loc3_.__isenum)
            {
               return null;
            }
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            if(param1 == "Bool")
            {
               return Boolean;
            }
            return null;
         }
         if(_loc3_ == null || _loc3_.__ename__ == null)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §\x17]sD\x01§(param1:Class, param2:Array) : Object
      {
         switch(int(param2.length))
         {
            case 0:
               §§push(new param1());
               break;
            case 1:
               §§push(new param1(param2[0]));
               break;
            case 2:
               §§push(new param1(param2[0],param2[1]));
               break;
            case 3:
               §§push(new param1(param2[0],param2[1],param2[2]));
               break;
            case 4:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3]));
               break;
            case 5:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4]));
               break;
            case 6:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5]));
               break;
            case 7:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6]));
               break;
            case 8:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7]));
               break;
            case 9:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8]));
               break;
            case 10:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9]));
               break;
            case 11:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10]));
               break;
            case 12:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11]));
               break;
            case 13:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11],param2[12]));
               break;
            case 14:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11],param2[12],param2[13]));
         }
         return §§pop();
      }
      
      public static function §\x04VVv\x02§(param1:Class) : Object
      {
         var _loc3_:* = null as Object;
         try
         {
            §\bRQ\f§.§U4V9\x03§ = true;
            _loc3_ = new param1();
            §\bRQ\f§.§U4V9\x03§ = false;
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            §\bRQ\f§.§U4V9\x03§ = false;
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw null;
         }
         return null;
      }
      
      public static function §\x1d*-X\x02§(param1:Class, param2:String, param3:Array = undefined) : Object
      {
         var _loc4_:Object = param1 == null?null:param1[param2];
         if(_loc4_ == null)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw "No such constructor " + param2;
         }
         if(§lM*9\x02§.§K\x0e0\x12\x02§(_loc4_))
         {
            if(param3 == null)
            {
               §\bRQ\f§.§\x07Bu#\x02§ = new Error();
               throw "Constructor " + param2 + " need parameters";
            }
            return _loc4_.apply(param1,param3);
         }
         if(param3 != null && int(param3.length) != 0)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw "Constructor " + param2 + " does not need parameters";
         }
         return _loc4_;
      }
      
      public static function §=Imo§(param1:Class, param2:int, param3:Array = undefined) : Object
      {
         var _loc4_:String = param1.__constructs__[param2];
         if(_loc4_ == null)
         {
            §\bRQ\f§.§\x07Bu#\x02§ = new Error();
            throw param2 + " is not a valid enum constructor index";
         }
         return §1`k§.§\x1d*-X\x02§(param1,_loc4_,param3);
      }
      
      public static function §(4l/\x01§(param1:*, param2:Boolean) : Array
      {
         var _loc8_:int = 0;
         var _loc3_:Array = [];
         var _loc4_:XML = describeType(param1);
         if(param2)
         {
            _loc4_ = _loc4_.factory[0];
         }
         var _loc5_:XMLList = _loc4_.child("method");
         var _loc6_:int = 0;
         var _loc7_:int = int(_loc5_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§j\x14\x17\x01§.§{t'\x0b\x03§(_loc5_[_loc8_].attribute("name")));
         }
         var _loc9_:XMLList = _loc4_.child("variable");
         _loc6_ = 0;
         _loc7_ = int(_loc9_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§j\x14\x17\x01§.§{t'\x0b\x03§(_loc9_[_loc8_].attribute("name")));
         }
         var _loc10_:XMLList = _loc4_.child("accessor");
         _loc6_ = 0;
         _loc7_ = int(_loc10_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§j\x14\x17\x01§.§{t'\x0b\x03§(_loc10_[_loc8_].attribute("name")));
         }
         return _loc3_;
      }
      
      public static function §4)63\x03§(param1:Class) : Array
      {
         return §1`k§.§(4l/\x01§(param1,true);
      }
      
      public static function §Hl|\x07\x01§(param1:Class) : Array
      {
         var _loc2_:Array = §1`k§.§(4l/\x01§(param1,false);
         _loc2_.§fl@@\x02§("__construct__");
         _loc2_.§fl@@\x02§("prototype");
         return _loc2_;
      }
      
      public static function §L\x12ty\x01§(param1:Class) : Array
      {
         var _loc2_:Array = param1.__constructs__;
         return _loc2_.§` V\x11§();
      }
      
      public static function §%\x055§(param1:*) : §M)MH\x01§
      {
         var _loc5_:* = null;
         var _loc3_:String = getQualifiedClassName(param1);
         var _loc4_:String = _loc3_;
         if(_loc4_ == "null")
         {
            return §M)MH\x01§.§h\x13\x18A§;
         }
         if(_loc4_ == "void")
         {
            return §M)MH\x01§.§h\x13\x18A§;
         }
         if(_loc4_ == "int")
         {
            return §M)MH\x01§.§Vt\x0f\x1f§;
         }
         if(_loc4_ == "Number")
         {
            if((param1 < -268435456 || param1 >= 268435456) && int(param1) == param1)
            {
               return §M)MH\x01§.§Vt\x0f\x1f§;
            }
            return §M)MH\x01§.§f\x16X\x07\x02§;
         }
         if(_loc4_ == "Boolean")
         {
            return §M)MH\x01§.§\x1eP~-§;
         }
         if(_loc4_ == "Object")
         {
            return §M)MH\x01§.§_\t)\x05\x03§;
         }
         if(_loc4_ == "Function")
         {
            return §M)MH\x01§.§u;wa\x02§;
         }
         _loc5_ = null;
         try
         {
            _loc5_ = getDefinitionByName(_loc3_);
            if(param1.hasOwnProperty("prototype"))
            {
               return §M)MH\x01§.§_\t)\x05\x03§;
            }
            if(_loc5_.__isenum)
            {
               return §M)MH\x01§.§wct3\x01§(_loc5_);
            }
            return §M)MH\x01§.§\x14\x03SV\x01§(_loc5_);
         }
         catch(_loc6_:*)
         {
            if(null as Error)
            {
               §\bRQ\f§.§\x07Bu#\x02§ = null;
            }
            if(_loc3_ == "builtin.as$0::MethodClosure" || int(_loc3_.indexOf("-")) != -1)
            {
               return §M)MH\x01§.§u;wa\x02§;
            }
            return _loc5_ == null?§M)MH\x01§.§u;wa\x02§:§M)MH\x01§.§\x14\x03SV\x01§(_loc5_);
         }
         return null;
      }
      
      public static function §%9\x18(\x02§(param1:Object, param2:Object) : Boolean
      {
         var _loc4_:* = null as Array;
         var _loc5_:* = null as Array;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         if(param1 == param2)
         {
            return true;
         }
         try
         {
            if(param1.§)-Z\x19\x01§ != param2.§)-Z\x19\x01§)
            {
               return false;
            }
            _loc4_ = param1.§7\x02@d\x02§;
            _loc5_ = param2.§7\x02@d\x02§;
            _loc6_ = 0;
            _loc7_ = int(_loc4_.length);
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               if(!§1`k§.§%9\x18(\x02§(_loc4_[_loc8_],_loc5_[_loc8_]))
               {
                  return false;
               }
            }
         }
         catch(_loc9_:*)
         {
            return false;
         }
         return true;
      }
      
      public static function §*cG}\x03§(param1:*) : String
      {
         return param1.§\x14s\x1e\x01§;
      }
      
      public static function §w!!\x13\x01§(param1:*) : Array
      {
         return param1.§7\x02@d\x02§ == null?[]:param1.§7\x02@d\x02§;
      }
      
      public static function §\x1cdv1\x01§(param1:*) : int
      {
         return param1.§)-Z\x19\x01§;
      }
   }
}
