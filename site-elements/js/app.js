var $hxClasses = $hxClasses || {},$estr = function() { return js.Boot.__string_rec(this,''); };
var Common = $hxClasses["Common"] = function() { }
Common.__name__ = ["Common"];
Common.host = function(name) {
	var host = mt.db.Phoneme.removeAccentsUTF8(name).toLowerCase();
	host = StringTools.trim(host);
	host = new EReg("\\s+","g").replace(host,"-");
	host = new EReg("[^a-z0-9.-]+","g").replace(host,"");
	if(host.length > 16) {
		var parts = host.split("-");
		if(parts[0].length >= 8) host = parts[0]; else {
			host = HxOverrides.substr(host,0,16);
			if(host.charAt(host.length - 1) == "-") host = HxOverrides.substr(host,0,host.length - 1);
		}
	}
	return host;
}
Common.hostValid = function(h) {
	return h.length >= 3 && h != "www" && h != "local" && h != "data";
}
var EReg = $hxClasses["EReg"] = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
EReg.__name__ = ["EReg"];
EReg.prototype = {
	customReplace: function(s,f) {
		var buf = new StringBuf();
		while(true) {
			if(!this.match(s)) break;
			buf.b += Std.string(this.matchedLeft());
			buf.b += Std.string(f(this));
			s = this.matchedRight();
		}
		buf.b += Std.string(s);
		return buf.b;
	}
	,replace: function(s,by) {
		return s.replace(this.r,by);
	}
	,split: function(s) {
		var d = "#__delim__#";
		return s.replace(this.r,d).split(d);
	}
	,matchedPos: function() {
		if(this.r.m == null) throw "No string matched";
		return { pos : this.r.m.index, len : this.r.m[0].length};
	}
	,matchedRight: function() {
		if(this.r.m == null) throw "No string matched";
		var sz = this.r.m.index + this.r.m[0].length;
		return this.r.s.substr(sz,this.r.s.length - sz);
	}
	,matchedLeft: function() {
		if(this.r.m == null) throw "No string matched";
		return this.r.s.substr(0,this.r.m.index);
	}
	,matched: function(n) {
		return this.r.m != null && n >= 0 && n < this.r.m.length?this.r.m[n]:(function($this) {
			var $r;
			throw "EReg::matched";
			return $r;
		}(this));
	}
	,match: function(s) {
		if(this.r.global) this.r.lastIndex = 0;
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,r: null
	,__class__: EReg
}
var Hash = $hxClasses["Hash"] = function() {
	this.h = { };
};
Hash.__name__ = ["Hash"];
Hash.prototype = {
	toString: function() {
		var s = new StringBuf();
		s.b += "{";
		var it = this.keys();
		while( it.hasNext() ) {
			var i = it.next();
			s.b += Std.string(i);
			s.b += " => ";
			s.b += Std.string(Std.string(this.get(i)));
			if(it.hasNext()) s.b += ", ";
		}
		s.b += "}";
		return s.b;
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref["$" + i];
		}};
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key.substr(1));
		}
		return HxOverrides.iter(a);
	}
	,remove: function(key) {
		key = "$" + key;
		if(!this.h.hasOwnProperty(key)) return false;
		delete(this.h[key]);
		return true;
	}
	,exists: function(key) {
		return this.h.hasOwnProperty("$" + key);
	}
	,get: function(key) {
		return this.h["$" + key];
	}
	,set: function(key,value) {
		this.h["$" + key] = value;
	}
	,h: null
	,__class__: Hash
}
var HxOverrides = $hxClasses["HxOverrides"] = function() { }
HxOverrides.__name__ = ["HxOverrides"];
HxOverrides.dateStr = function(date) {
	var m = date.getMonth() + 1;
	var d = date.getDate();
	var h = date.getHours();
	var mi = date.getMinutes();
	var s = date.getSeconds();
	return date.getFullYear() + "-" + (m < 10?"0" + m:"" + m) + "-" + (d < 10?"0" + d:"" + d) + " " + (h < 10?"0" + h:"" + h) + ":" + (mi < 10?"0" + mi:"" + mi) + ":" + (s < 10?"0" + s:"" + s);
}
HxOverrides.strDate = function(s) {
	switch(s.length) {
	case 8:
		var k = s.split(":");
		var d = new Date();
		d.setTime(0);
		d.setUTCHours(k[0]);
		d.setUTCMinutes(k[1]);
		d.setUTCSeconds(k[2]);
		return d;
	case 10:
		var k = s.split("-");
		return new Date(k[0],k[1] - 1,k[2],0,0,0);
	case 19:
		var k = s.split(" ");
		var y = k[0].split("-");
		var t = k[1].split(":");
		return new Date(y[0],y[1] - 1,y[2],t[0],t[1],t[2]);
	default:
		throw "Invalid date format : " + s;
	}
}
HxOverrides.cca = function(s,index) {
	var x = s.cca(index);
	if(x != x) return undefined;
	return x;
}
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
}
HxOverrides.remove = function(a,obj) {
	var i = 0;
	var l = a.length;
	while(i < l) {
		if(a[i] == obj) {
			a.splice(i,1);
			return true;
		}
		i++;
	}
	return false;
}
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
}
var IntHash = $hxClasses["IntHash"] = function() {
	this.h = { };
};
IntHash.__name__ = ["IntHash"];
IntHash.prototype = {
	toString: function() {
		var s = new StringBuf();
		s.b += "{";
		var it = this.keys();
		while( it.hasNext() ) {
			var i = it.next();
			s.b += Std.string(i);
			s.b += " => ";
			s.b += Std.string(Std.string(this.get(i)));
			if(it.hasNext()) s.b += ", ";
		}
		s.b += "}";
		return s.b;
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref[i];
		}};
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key | 0);
		}
		return HxOverrides.iter(a);
	}
	,remove: function(key) {
		if(!this.h.hasOwnProperty(key)) return false;
		delete(this.h[key]);
		return true;
	}
	,exists: function(key) {
		return this.h.hasOwnProperty(key);
	}
	,get: function(key) {
		return this.h[key];
	}
	,set: function(key,value) {
		this.h[key] = value;
	}
	,h: null
	,__class__: IntHash
}
var IntIter = $hxClasses["IntIter"] = function(min,max) {
	this.min = min;
	this.max = max;
};
IntIter.__name__ = ["IntIter"];
IntIter.prototype = {
	next: function() {
		return this.min++;
	}
	,hasNext: function() {
		return this.min < this.max;
	}
	,max: null
	,min: null
	,__class__: IntIter
}
var Lambda = $hxClasses["Lambda"] = function() { }
Lambda.__name__ = ["Lambda"];
Lambda.array = function(it) {
	var a = new Array();
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var i = $it0.next();
		a.push(i);
	}
	return a;
}
Lambda.list = function(it) {
	var l = new List();
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var i = $it0.next();
		l.add(i);
	}
	return l;
}
Lambda.map = function(it,f) {
	var l = new List();
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		l.add(f(x));
	}
	return l;
}
Lambda.mapi = function(it,f) {
	var l = new List();
	var i = 0;
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		l.add(f(i++,x));
	}
	return l;
}
Lambda.has = function(it,elt,cmp) {
	if(cmp == null) {
		var $it0 = $iterator(it)();
		while( $it0.hasNext() ) {
			var x = $it0.next();
			if(x == elt) return true;
		}
	} else {
		var $it1 = $iterator(it)();
		while( $it1.hasNext() ) {
			var x = $it1.next();
			if(cmp(x,elt)) return true;
		}
	}
	return false;
}
Lambda.exists = function(it,f) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		if(f(x)) return true;
	}
	return false;
}
Lambda.foreach = function(it,f) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		if(!f(x)) return false;
	}
	return true;
}
Lambda.iter = function(it,f) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		f(x);
	}
}
Lambda.filter = function(it,f) {
	var l = new List();
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		if(f(x)) l.add(x);
	}
	return l;
}
Lambda.fold = function(it,f,first) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		first = f(x,first);
	}
	return first;
}
Lambda.count = function(it,pred) {
	var n = 0;
	if(pred == null) {
		var $it0 = $iterator(it)();
		while( $it0.hasNext() ) {
			var _ = $it0.next();
			n++;
		}
	} else {
		var $it1 = $iterator(it)();
		while( $it1.hasNext() ) {
			var x = $it1.next();
			if(pred(x)) n++;
		}
	}
	return n;
}
Lambda.empty = function(it) {
	return !$iterator(it)().hasNext();
}
Lambda.indexOf = function(it,v) {
	var i = 0;
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var v2 = $it0.next();
		if(v == v2) return i;
		i++;
	}
	return -1;
}
Lambda.concat = function(a,b) {
	var l = new List();
	var $it0 = $iterator(a)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		l.add(x);
	}
	var $it1 = $iterator(b)();
	while( $it1.hasNext() ) {
		var x = $it1.next();
		l.add(x);
	}
	return l;
}
var List = $hxClasses["List"] = function() {
	this.length = 0;
};
List.__name__ = ["List"];
List.prototype = {
	map: function(f) {
		var b = new List();
		var l = this.h;
		while(l != null) {
			var v = l[0];
			l = l[1];
			b.add(f(v));
		}
		return b;
	}
	,filter: function(f) {
		var l2 = new List();
		var l = this.h;
		while(l != null) {
			var v = l[0];
			l = l[1];
			if(f(v)) l2.add(v);
		}
		return l2;
	}
	,join: function(sep) {
		var s = new StringBuf();
		var first = true;
		var l = this.h;
		while(l != null) {
			if(first) first = false; else s.b += Std.string(sep);
			s.b += Std.string(l[0]);
			l = l[1];
		}
		return s.b;
	}
	,toString: function() {
		var s = new StringBuf();
		var first = true;
		var l = this.h;
		s.b += "{";
		while(l != null) {
			if(first) first = false; else s.b += ", ";
			s.b += Std.string(Std.string(l[0]));
			l = l[1];
		}
		s.b += "}";
		return s.b;
	}
	,iterator: function() {
		return { h : this.h, hasNext : function() {
			return this.h != null;
		}, next : function() {
			if(this.h == null) return null;
			var x = this.h[0];
			this.h = this.h[1];
			return x;
		}};
	}
	,remove: function(v) {
		var prev = null;
		var l = this.h;
		while(l != null) {
			if(l[0] == v) {
				if(prev == null) this.h = l[1]; else prev[1] = l[1];
				if(this.q == l) this.q = prev;
				this.length--;
				return true;
			}
			prev = l;
			l = l[1];
		}
		return false;
	}
	,clear: function() {
		this.h = null;
		this.q = null;
		this.length = 0;
	}
	,isEmpty: function() {
		return this.h == null;
	}
	,pop: function() {
		if(this.h == null) return null;
		var x = this.h[0];
		this.h = this.h[1];
		if(this.h == null) this.q = null;
		this.length--;
		return x;
	}
	,last: function() {
		return this.q == null?null:this.q[0];
	}
	,first: function() {
		return this.h == null?null:this.h[0];
	}
	,push: function(item) {
		var x = [item,this.h];
		this.h = x;
		if(this.q == null) this.q = x;
		this.length++;
	}
	,add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,length: null
	,q: null
	,h: null
	,__class__: List
}
var Std = $hxClasses["Std"] = function() { }
Std.__name__ = ["Std"];
Std["is"] = function(v,t) {
	return js.Boot.__instanceof(v,t);
}
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
}
Std["int"] = function(x) {
	return x | 0;
}
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
}
Std.parseFloat = function(x) {
	return parseFloat(x);
}
Std.random = function(x) {
	return x <= 0?0:Math.floor(Math.random() * x);
}
var StringBuf = $hxClasses["StringBuf"] = function() {
	this.b = "";
};
StringBuf.__name__ = ["StringBuf"];
StringBuf.prototype = {
	toString: function() {
		return this.b;
	}
	,addSub: function(s,pos,len) {
		this.b += HxOverrides.substr(s,pos,len);
	}
	,addChar: function(c) {
		this.b += String.fromCharCode(c);
	}
	,add: function(x) {
		this.b += Std.string(x);
	}
	,b: null
	,__class__: StringBuf
}
var StringTools = $hxClasses["StringTools"] = function() { }
StringTools.__name__ = ["StringTools"];
StringTools.urlEncode = function(s) {
	return encodeURIComponent(s);
}
StringTools.urlDecode = function(s) {
	return decodeURIComponent(s.split("+").join(" "));
}
StringTools.htmlEscape = function(s,quotes) {
	s = s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
	return quotes?s.split("\"").join("&quot;").split("'").join("&#039;"):s;
}
StringTools.htmlUnescape = function(s) {
	return s.split("&gt;").join(">").split("&lt;").join("<").split("&quot;").join("\"").split("&#039;").join("'").split("&amp;").join("&");
}
StringTools.startsWith = function(s,start) {
	return s.length >= start.length && HxOverrides.substr(s,0,start.length) == start;
}
StringTools.endsWith = function(s,end) {
	var elen = end.length;
	var slen = s.length;
	return slen >= elen && HxOverrides.substr(s,slen - elen,elen) == end;
}
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	return c >= 9 && c <= 13 || c == 32;
}
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) r++;
	if(r > 0) return HxOverrides.substr(s,r,l - r); else return s;
}
StringTools.rtrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,l - r - 1)) r++;
	if(r > 0) return HxOverrides.substr(s,0,l - r); else return s;
}
StringTools.trim = function(s) {
	return StringTools.ltrim(StringTools.rtrim(s));
}
StringTools.rpad = function(s,c,l) {
	var sl = s.length;
	var cl = c.length;
	while(sl < l) if(l - sl < cl) {
		s += HxOverrides.substr(c,0,l - sl);
		sl = l;
	} else {
		s += c;
		sl += cl;
	}
	return s;
}
StringTools.lpad = function(s,c,l) {
	var ns = "";
	var sl = s.length;
	if(sl >= l) return s;
	var cl = c.length;
	while(sl < l) if(l - sl < cl) {
		ns += HxOverrides.substr(c,0,l - sl);
		sl = l;
	} else {
		ns += c;
		sl += cl;
	}
	return ns + s;
}
StringTools.replace = function(s,sub,by) {
	return s.split(sub).join(by);
}
StringTools.hex = function(n,digits) {
	var s = "";
	var hexChars = "0123456789ABCDEF";
	do {
		s = hexChars.charAt(n & 15) + s;
		n >>>= 4;
	} while(n > 0);
	if(digits != null) while(s.length < digits) s = "0" + s;
	return s;
}
StringTools.fastCodeAt = function(s,index) {
	return s.cca(index);
}
StringTools.isEOF = function(c) {
	return c != c;
}
var haxe = haxe || {}
haxe.Http = $hxClasses["haxe.Http"] = function(url) {
	this.url = url;
	this.headers = new Hash();
	this.params = new Hash();
	this.async = true;
};
haxe.Http.__name__ = ["haxe","Http"];
haxe.Http.requestUrl = function(url) {
	var h = new haxe.Http(url);
	h.async = false;
	var r = null;
	h.onData = function(d) {
		r = d;
	};
	h.onError = function(e) {
		throw e;
	};
	h.request(false);
	return r;
}
haxe.Http.prototype = {
	onStatus: function(status) {
	}
	,onError: function(msg) {
	}
	,onData: function(data) {
	}
	,request: function(post) {
		var me = this;
		var r = new js.XMLHttpRequest();
		var onreadystatechange = function() {
			if(r.readyState != 4) return;
			var s = (function($this) {
				var $r;
				try {
					$r = r.status;
				} catch( e ) {
					$r = null;
				}
				return $r;
			}(this));
			if(s == undefined) s = null;
			if(s != null) me.onStatus(s);
			if(s != null && s >= 200 && s < 400) me.onData(r.responseText); else switch(s) {
			case null: case undefined:
				me.onError("Failed to connect or resolve host");
				break;
			case 12029:
				me.onError("Failed to connect to host");
				break;
			case 12007:
				me.onError("Unknown host");
				break;
			default:
				me.onError("Http Error #" + r.status);
			}
		};
		if(this.async) r.onreadystatechange = onreadystatechange;
		var uri = this.postData;
		if(uri != null) post = true; else {
			var $it0 = this.params.keys();
			while( $it0.hasNext() ) {
				var p = $it0.next();
				if(uri == null) uri = ""; else uri += "&";
				uri += StringTools.urlEncode(p) + "=" + StringTools.urlEncode(this.params.get(p));
			}
		}
		try {
			if(post) r.open("POST",this.url,this.async); else if(uri != null) {
				var question = this.url.split("?").length <= 1;
				r.open("GET",this.url + (question?"?":"&") + uri,this.async);
				uri = null;
			} else r.open("GET",this.url,this.async);
		} catch( e ) {
			this.onError(e.toString());
			return;
		}
		if(this.headers.get("Content-Type") == null && post && this.postData == null) r.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		var $it1 = this.headers.keys();
		while( $it1.hasNext() ) {
			var h = $it1.next();
			r.setRequestHeader(h,this.headers.get(h));
		}
		r.send(uri);
		if(!this.async) onreadystatechange();
	}
	,setPostData: function(data) {
		this.postData = data;
	}
	,setParameter: function(param,value) {
		this.params.set(param,value);
	}
	,setHeader: function(header,value) {
		this.headers.set(header,value);
	}
	,params: null
	,headers: null
	,postData: null
	,async: null
	,url: null
	,__class__: haxe.Http
}
haxe.Log = $hxClasses["haxe.Log"] = function() { }
haxe.Log.__name__ = ["haxe","Log"];
haxe.Log.trace = function(v,infos) {
	js.Boot.__trace(v,infos);
}
haxe.Log.clear = function() {
	js.Boot.__clear_trace();
}
haxe.Md5 = $hxClasses["haxe.Md5"] = function() {
};
haxe.Md5.__name__ = ["haxe","Md5"];
haxe.Md5.encode = function(s) {
	return new haxe.Md5().doEncode(s);
}
haxe.Md5.prototype = {
	doEncode: function(str) {
		var x = this.str2blks(str);
		var a = 1732584193;
		var b = -271733879;
		var c = -1732584194;
		var d = 271733878;
		var step;
		var i = 0;
		while(i < x.length) {
			var olda = a;
			var oldb = b;
			var oldc = c;
			var oldd = d;
			step = 0;
			a = this.ff(a,b,c,d,x[i],7,-680876936);
			d = this.ff(d,a,b,c,x[i + 1],12,-389564586);
			c = this.ff(c,d,a,b,x[i + 2],17,606105819);
			b = this.ff(b,c,d,a,x[i + 3],22,-1044525330);
			a = this.ff(a,b,c,d,x[i + 4],7,-176418897);
			d = this.ff(d,a,b,c,x[i + 5],12,1200080426);
			c = this.ff(c,d,a,b,x[i + 6],17,-1473231341);
			b = this.ff(b,c,d,a,x[i + 7],22,-45705983);
			a = this.ff(a,b,c,d,x[i + 8],7,1770035416);
			d = this.ff(d,a,b,c,x[i + 9],12,-1958414417);
			c = this.ff(c,d,a,b,x[i + 10],17,-42063);
			b = this.ff(b,c,d,a,x[i + 11],22,-1990404162);
			a = this.ff(a,b,c,d,x[i + 12],7,1804603682);
			d = this.ff(d,a,b,c,x[i + 13],12,-40341101);
			c = this.ff(c,d,a,b,x[i + 14],17,-1502002290);
			b = this.ff(b,c,d,a,x[i + 15],22,1236535329);
			a = this.gg(a,b,c,d,x[i + 1],5,-165796510);
			d = this.gg(d,a,b,c,x[i + 6],9,-1069501632);
			c = this.gg(c,d,a,b,x[i + 11],14,643717713);
			b = this.gg(b,c,d,a,x[i],20,-373897302);
			a = this.gg(a,b,c,d,x[i + 5],5,-701558691);
			d = this.gg(d,a,b,c,x[i + 10],9,38016083);
			c = this.gg(c,d,a,b,x[i + 15],14,-660478335);
			b = this.gg(b,c,d,a,x[i + 4],20,-405537848);
			a = this.gg(a,b,c,d,x[i + 9],5,568446438);
			d = this.gg(d,a,b,c,x[i + 14],9,-1019803690);
			c = this.gg(c,d,a,b,x[i + 3],14,-187363961);
			b = this.gg(b,c,d,a,x[i + 8],20,1163531501);
			a = this.gg(a,b,c,d,x[i + 13],5,-1444681467);
			d = this.gg(d,a,b,c,x[i + 2],9,-51403784);
			c = this.gg(c,d,a,b,x[i + 7],14,1735328473);
			b = this.gg(b,c,d,a,x[i + 12],20,-1926607734);
			a = this.hh(a,b,c,d,x[i + 5],4,-378558);
			d = this.hh(d,a,b,c,x[i + 8],11,-2022574463);
			c = this.hh(c,d,a,b,x[i + 11],16,1839030562);
			b = this.hh(b,c,d,a,x[i + 14],23,-35309556);
			a = this.hh(a,b,c,d,x[i + 1],4,-1530992060);
			d = this.hh(d,a,b,c,x[i + 4],11,1272893353);
			c = this.hh(c,d,a,b,x[i + 7],16,-155497632);
			b = this.hh(b,c,d,a,x[i + 10],23,-1094730640);
			a = this.hh(a,b,c,d,x[i + 13],4,681279174);
			d = this.hh(d,a,b,c,x[i],11,-358537222);
			c = this.hh(c,d,a,b,x[i + 3],16,-722521979);
			b = this.hh(b,c,d,a,x[i + 6],23,76029189);
			a = this.hh(a,b,c,d,x[i + 9],4,-640364487);
			d = this.hh(d,a,b,c,x[i + 12],11,-421815835);
			c = this.hh(c,d,a,b,x[i + 15],16,530742520);
			b = this.hh(b,c,d,a,x[i + 2],23,-995338651);
			a = this.ii(a,b,c,d,x[i],6,-198630844);
			d = this.ii(d,a,b,c,x[i + 7],10,1126891415);
			c = this.ii(c,d,a,b,x[i + 14],15,-1416354905);
			b = this.ii(b,c,d,a,x[i + 5],21,-57434055);
			a = this.ii(a,b,c,d,x[i + 12],6,1700485571);
			d = this.ii(d,a,b,c,x[i + 3],10,-1894986606);
			c = this.ii(c,d,a,b,x[i + 10],15,-1051523);
			b = this.ii(b,c,d,a,x[i + 1],21,-2054922799);
			a = this.ii(a,b,c,d,x[i + 8],6,1873313359);
			d = this.ii(d,a,b,c,x[i + 15],10,-30611744);
			c = this.ii(c,d,a,b,x[i + 6],15,-1560198380);
			b = this.ii(b,c,d,a,x[i + 13],21,1309151649);
			a = this.ii(a,b,c,d,x[i + 4],6,-145523070);
			d = this.ii(d,a,b,c,x[i + 11],10,-1120210379);
			c = this.ii(c,d,a,b,x[i + 2],15,718787259);
			b = this.ii(b,c,d,a,x[i + 9],21,-343485551);
			a = this.addme(a,olda);
			b = this.addme(b,oldb);
			c = this.addme(c,oldc);
			d = this.addme(d,oldd);
			i += 16;
		}
		return this.rhex(a) + this.rhex(b) + this.rhex(c) + this.rhex(d);
	}
	,ii: function(a,b,c,d,x,s,t) {
		return this.cmn(this.bitXOR(c,this.bitOR(b,~d)),a,b,x,s,t);
	}
	,hh: function(a,b,c,d,x,s,t) {
		return this.cmn(this.bitXOR(this.bitXOR(b,c),d),a,b,x,s,t);
	}
	,gg: function(a,b,c,d,x,s,t) {
		return this.cmn(this.bitOR(this.bitAND(b,d),this.bitAND(c,~d)),a,b,x,s,t);
	}
	,ff: function(a,b,c,d,x,s,t) {
		return this.cmn(this.bitOR(this.bitAND(b,c),this.bitAND(~b,d)),a,b,x,s,t);
	}
	,cmn: function(q,a,b,x,s,t) {
		return this.addme(this.rol(this.addme(this.addme(a,q),this.addme(x,t)),s),b);
	}
	,rol: function(num,cnt) {
		return num << cnt | num >>> 32 - cnt;
	}
	,str2blks: function(str) {
		var nblk = (str.length + 8 >> 6) + 1;
		var blks = new Array();
		var blksSize = nblk * 16;
		var _g = 0;
		while(_g < blksSize) {
			var i = _g++;
			blks[i] = 0;
		}
		var i = 0;
		while(i < str.length) {
			blks[i >> 2] |= HxOverrides.cca(str,i) << (str.length * 8 + i) % 4 * 8;
			i++;
		}
		blks[i >> 2] |= 128 << (str.length * 8 + i) % 4 * 8;
		var l = str.length * 8;
		var k = nblk * 16 - 2;
		blks[k] = l & 255;
		blks[k] |= (l >>> 8 & 255) << 8;
		blks[k] |= (l >>> 16 & 255) << 16;
		blks[k] |= (l >>> 24 & 255) << 24;
		return blks;
	}
	,rhex: function(num) {
		var str = "";
		var hex_chr = "0123456789abcdef";
		var _g = 0;
		while(_g < 4) {
			var j = _g++;
			str += hex_chr.charAt(num >> j * 8 + 4 & 15) + hex_chr.charAt(num >> j * 8 & 15);
		}
		return str;
	}
	,addme: function(x,y) {
		var lsw = (x & 65535) + (y & 65535);
		var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
		return msw << 16 | lsw & 65535;
	}
	,bitAND: function(a,b) {
		var lsb = a & 1 & (b & 1);
		var msb31 = a >>> 1 & b >>> 1;
		return msb31 << 1 | lsb;
	}
	,bitXOR: function(a,b) {
		var lsb = a & 1 ^ b & 1;
		var msb31 = a >>> 1 ^ b >>> 1;
		return msb31 << 1 | lsb;
	}
	,bitOR: function(a,b) {
		var lsb = a & 1 | b & 1;
		var msb31 = a >>> 1 | b >>> 1;
		return msb31 << 1 | lsb;
	}
	,__class__: haxe.Md5
}
haxe.Timer = $hxClasses["haxe.Timer"] = function(time_ms) {
	var me = this;
	this.id = setInterval(function() {
		me.run();
	},time_ms);
};
haxe.Timer.__name__ = ["haxe","Timer"];
haxe.Timer.delay = function(f,time_ms) {
	var t = new haxe.Timer(time_ms);
	t.run = function() {
		t.stop();
		f();
	};
	return t;
}
haxe.Timer.measure = function(f,pos) {
	var t0 = haxe.Timer.stamp();
	var r = f();
	haxe.Log.trace(haxe.Timer.stamp() - t0 + "s",pos);
	return r;
}
haxe.Timer.stamp = function() {
	return new Date().getTime() / 1000;
}
haxe.Timer.prototype = {
	run: function() {
	}
	,stop: function() {
		if(this.id == null) return;
		clearInterval(this.id);
		this.id = null;
	}
	,id: null
	,__class__: haxe.Timer
}
if(!haxe.io) haxe.io = {}
haxe.io.Bytes = $hxClasses["haxe.io.Bytes"] = function(length,b) {
	this.length = length;
	this.b = b;
};
haxe.io.Bytes.__name__ = ["haxe","io","Bytes"];
haxe.io.Bytes.alloc = function(length) {
	var a = new Array();
	var _g = 0;
	while(_g < length) {
		var i = _g++;
		a.push(0);
	}
	return new haxe.io.Bytes(length,a);
}
haxe.io.Bytes.ofString = function(s) {
	var a = new Array();
	var _g1 = 0, _g = s.length;
	while(_g1 < _g) {
		var i = _g1++;
		var c = s.cca(i);
		if(c <= 127) a.push(c); else if(c <= 2047) {
			a.push(192 | c >> 6);
			a.push(128 | c & 63);
		} else if(c <= 65535) {
			a.push(224 | c >> 12);
			a.push(128 | c >> 6 & 63);
			a.push(128 | c & 63);
		} else {
			a.push(240 | c >> 18);
			a.push(128 | c >> 12 & 63);
			a.push(128 | c >> 6 & 63);
			a.push(128 | c & 63);
		}
	}
	return new haxe.io.Bytes(a.length,a);
}
haxe.io.Bytes.ofData = function(b) {
	return new haxe.io.Bytes(b.length,b);
}
haxe.io.Bytes.fastGet = function(b,pos) {
	return b[pos];
}
haxe.io.Bytes.prototype = {
	getData: function() {
		return this.b;
	}
	,toHex: function() {
		var s = new StringBuf();
		var chars = [];
		var str = "0123456789abcdef";
		var _g1 = 0, _g = str.length;
		while(_g1 < _g) {
			var i = _g1++;
			chars.push(HxOverrides.cca(str,i));
		}
		var _g1 = 0, _g = this.length;
		while(_g1 < _g) {
			var i = _g1++;
			var c = this.b[i];
			s.b += String.fromCharCode(chars[c >> 4]);
			s.b += String.fromCharCode(chars[c & 15]);
		}
		return s.b;
	}
	,toString: function() {
		return this.readString(0,this.length);
	}
	,readString: function(pos,len) {
		if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io.Error.OutsideBounds;
		var s = "";
		var b = this.b;
		var fcc = String.fromCharCode;
		var i = pos;
		var max = pos + len;
		while(i < max) {
			var c = b[i++];
			if(c < 128) {
				if(c == 0) break;
				s += fcc(c);
			} else if(c < 224) s += fcc((c & 63) << 6 | b[i++] & 127); else if(c < 240) {
				var c2 = b[i++];
				s += fcc((c & 31) << 12 | (c2 & 127) << 6 | b[i++] & 127);
			} else {
				var c2 = b[i++];
				var c3 = b[i++];
				s += fcc((c & 15) << 18 | (c2 & 127) << 12 | c3 << 6 & 127 | b[i++] & 127);
			}
		}
		return s;
	}
	,compare: function(other) {
		var b1 = this.b;
		var b2 = other.b;
		var len = this.length < other.length?this.length:other.length;
		var _g = 0;
		while(_g < len) {
			var i = _g++;
			if(b1[i] != b2[i]) return b1[i] - b2[i];
		}
		return this.length - other.length;
	}
	,sub: function(pos,len) {
		if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io.Error.OutsideBounds;
		return new haxe.io.Bytes(len,this.b.slice(pos,pos + len));
	}
	,blit: function(pos,src,srcpos,len) {
		if(pos < 0 || srcpos < 0 || len < 0 || pos + len > this.length || srcpos + len > src.length) throw haxe.io.Error.OutsideBounds;
		var b1 = this.b;
		var b2 = src.b;
		if(b1 == b2 && pos > srcpos) {
			var i = len;
			while(i > 0) {
				i--;
				b1[i + pos] = b2[i + srcpos];
			}
			return;
		}
		var _g = 0;
		while(_g < len) {
			var i = _g++;
			b1[i + pos] = b2[i + srcpos];
		}
	}
	,set: function(pos,v) {
		this.b[pos] = v & 255;
	}
	,get: function(pos) {
		return this.b[pos];
	}
	,b: null
	,length: null
	,__class__: haxe.io.Bytes
}
haxe.io.Error = $hxClasses["haxe.io.Error"] = { __ename__ : ["haxe","io","Error"], __constructs__ : ["Blocked","Overflow","OutsideBounds","Custom"] }
haxe.io.Error.Blocked = ["Blocked",0];
haxe.io.Error.Blocked.toString = $estr;
haxe.io.Error.Blocked.__enum__ = haxe.io.Error;
haxe.io.Error.Overflow = ["Overflow",1];
haxe.io.Error.Overflow.toString = $estr;
haxe.io.Error.Overflow.__enum__ = haxe.io.Error;
haxe.io.Error.OutsideBounds = ["OutsideBounds",2];
haxe.io.Error.OutsideBounds.toString = $estr;
haxe.io.Error.OutsideBounds.__enum__ = haxe.io.Error;
haxe.io.Error.Custom = function(e) { var $x = ["Custom",3,e]; $x.__enum__ = haxe.io.Error; $x.toString = $estr; return $x; }
var mt = mt || {}
if(!mt.js) mt.js = {}
mt.js.Tip = $hxClasses["mt.js.Tip"] = function() { }
mt.js.Tip.__name__ = ["mt","js","Tip"];
mt.js.Tip.lastRef = null;
mt.js.Tip.placeRef = null;
mt.js.Tip.initialized = null;
mt.js.Tip.tooltip = null;
mt.js.Tip.tooltipContent = null;
mt.js.Tip.mousePos = null;
mt.js.Tip.onHide = null;
mt.js.Tip.excludeList = null;
mt.js.Tip.show = function(refObj,contentHTML,cName,pRef) {
	mt.js.Tip.init();
	if(mt.js.Tip.tooltip == null) {
		mt.js.Tip.tooltip = js.Lib.document.getElementById(mt.js.Tip.tooltipId);
		if(mt.js.Tip.tooltip == null) {
			mt.js.Tip.tooltip = js.Lib.document.createElement("div");
			mt.js.Tip.tooltip.id = mt.js.Tip.tooltipId;
			js.Lib.document.body.insertBefore(mt.js.Tip.tooltip,js.Lib.document.body.firstChild);
		}
		mt.js.Tip.tooltip.style.top = "-1000px";
		mt.js.Tip.tooltip.style.position = "absolute";
		mt.js.Tip.tooltip.style.zIndex = mt.js.Tip.tipZIndex;
	}
	if(mt.js.Tip.tooltipContent == null) {
		mt.js.Tip.tooltipContent = js.Lib.document.getElementById(mt.js.Tip.tooltipContentId);
		if(mt.js.Tip.tooltipContent == null) {
			mt.js.Tip.tooltipContent = js.Lib.document.createElement("div");
			mt.js.Tip.tooltipContent.id = mt.js.Tip.tooltipContentId;
			mt.js.Tip.tooltip.appendChild(mt.js.Tip.tooltipContent);
		}
	}
	if(pRef == null) pRef = false;
	mt.js.Tip.placeRef = pRef;
	if(cName == null) mt.js.Tip.tooltip.className = mt.js.Tip.defaultClass; else mt.js.Tip.tooltip.className = cName;
	if(mt.js.Tip.lastRef != null && mt.js.Tip.onHide != null) {
		mt.js.Tip.onHide();
		mt.js.Tip.onHide = null;
	}
	mt.js.Tip.lastRef = refObj;
	mt.js.Tip.tooltipContent.innerHTML = contentHTML;
	if(mt.js.Tip.placeRef) mt.js.Tip.placeTooltipRef(); else mt.js.Tip.placeTooltip();
}
mt.js.Tip.exclude = function(id) {
	var e = js.Lib.document.getElementById(id);
	if(e == null) throw id + " not found";
	if(mt.js.Tip.excludeList == null) mt.js.Tip.excludeList = new List();
	mt.js.Tip.excludeList.add(e);
}
mt.js.Tip.placeTooltip = function() {
	if(mt.js.Tip.mousePos == null) return;
	var tts = mt.js.Tip.elementSize(mt.js.Tip.tooltip);
	var w = mt.js.Tip.windowSize();
	var top = 0;
	var left = 0;
	left = mt.js.Tip.mousePos.x + mt.js.Tip.xOffset;
	top = mt.js.Tip.mousePos.y + mt.js.Tip.yOffset;
	if(top + tts.height > w.height - 2 + w.scrollTop) {
		if(mt.js.Tip.mousePos.y - tts.height > 5 + w.scrollTop) top = mt.js.Tip.mousePos.y - tts.height - 5; else top = w.height - 2 + w.scrollTop - tts.height;
	}
	if(left + tts.width > w.width - 22 + w.scrollLeft) {
		if(mt.js.Tip.mousePos.x - tts.width > 5 + w.scrollLeft) left = mt.js.Tip.mousePos.x - tts.width - 5; else left = w.width - 22 + w.scrollLeft - tts.width;
	}
	if(top < 0) top = 0;
	if(left < 0) left = 0;
	if(mt.js.Tip.excludeList != null) {
		var $it0 = mt.js.Tip.excludeList.iterator();
		while( $it0.hasNext() ) {
			var e = $it0.next();
			var s = mt.js.Tip.elementSize(e);
			if(left > s.x + s.width || left + tts.width < s.x || top > s.y + s.height || top + tts.height < s.y) continue;
			var dx1 = left - (s.x + s.width);
			var dx2 = left + tts.width - s.x;
			var dx = Math.abs(dx1) > Math.abs(dx2)?dx2:dx1;
			var dy1 = top - (s.y + s.height);
			var dy2 = top + tts.height - s.y;
			var dy = Math.abs(dy1) > Math.abs(dy2)?dy2:dy1;
			var cx = left + tts.width / 2 - mt.js.Tip.mousePos.x;
			var cy = top + tts.height / 2 - mt.js.Tip.mousePos.y;
			if((cx - dx) * (cx - dx) + cy * cy > cx * cx + (cy - dy) * (cy - dy)) top -= dy; else left -= dx;
		}
	}
	mt.js.Tip.tooltip.style.left = left + "px";
	mt.js.Tip.tooltip.style.top = top + "px";
}
mt.js.Tip.placeTooltipRef = function() {
	var o = mt.js.Tip.elementSize(mt.js.Tip.lastRef);
	var tts = mt.js.Tip.elementSize(mt.js.Tip.tooltip);
	if(o.width <= 0) mt.js.Tip.tooltip.style.left = o.x + "px"; else mt.js.Tip.tooltip.style.left = o.x - tts.width * 0.5 + o.width * 0.5 + "px";
	mt.js.Tip.tooltip.style.top = o.y + Math.max(mt.js.Tip.minOffsetY,o.height) + "px";
}
mt.js.Tip.showTip = function(refObj,title,contentBase) {
	contentBase = "<p>" + contentBase + "</p>";
	mt.js.Tip.show(refObj,"<div class=\"title\">" + title + "</div>" + contentBase);
}
mt.js.Tip.hide = function() {
	if(mt.js.Tip.lastRef == null) return;
	mt.js.Tip.lastRef = null;
	if(mt.js.Tip.onHide != null) {
		mt.js.Tip.onHide();
		mt.js.Tip.onHide = null;
	}
	mt.js.Tip.tooltip.style.top = "-1000px";
	mt.js.Tip.tooltip.style.width = "";
}
mt.js.Tip.clean = function() {
	if(mt.js.Tip.lastRef == null) return;
	if(mt.js.Tip.lastRef.parentNode == null) return mt.js.Tip.hide();
	if(mt.js.Tip.lastRef.id != null && mt.js.Tip.lastRef.id != "") {
		if(js.Lib.document.getElementById(mt.js.Tip.lastRef.id) != mt.js.Tip.lastRef) return mt.js.Tip.hide();
	}
	return;
}
mt.js.Tip.elementSize = function(o) {
	var ret = { x : 0, y : 0, width : o.clientWidth, height : o.clientHeight};
	var p = o;
	while(p != null) {
		if(p.offsetParent != null) {
			ret.x += p.offsetLeft - p.scrollLeft;
			ret.y += p.offsetTop - p.scrollTop;
		} else {
			ret.x += p.offsetLeft;
			ret.y += p.offsetTop;
		}
		p = p.offsetParent;
	}
	return ret;
}
mt.js.Tip.windowSize = function() {
	var ret = { x : 0, y : 0, width : js.Lib.window.innerWidth, height : js.Lib.window.innerHeight, scrollLeft : js.Lib.document.body.scrollLeft + js.Lib.document.documentElement.scrollLeft, scrollTop : js.Lib.document.body.scrollTop + js.Lib.document.documentElement.scrollTop};
	var isIE = document.all != null && window.opera == null;
	var body = isIE?js.Lib.document.documentElement:js.Lib.document.body;
	if(ret.width == null) ret.width = body.clientWidth;
	if(ret.height == null) ret.height = body.clientHeight;
	return ret;
}
mt.js.Tip.onMouseMove = function(evt) {
	try {
		var posx = 0;
		var posy = 0;
		if(evt == null) evt = js.Lib.window.event;
		var e = evt;
		if(e.pageX || e.pageY) {
			posx = e.pageX;
			posy = e.pageY;
		} else if(e.clientX || e.clientY) {
			posx = e.clientX + js.Lib.document.body.scrollLeft + js.Lib.document.documentElement.scrollLeft;
			posy = e.clientY + js.Lib.document.body.scrollTop + js.Lib.document.documentElement.scrollTop;
		}
		mt.js.Tip.mousePos = { x : posx, y : posy};
		if(mt.js.Tip.lastRef != null && !mt.js.Tip.placeRef) mt.js.Tip.placeTooltip();
	} catch( e ) {
	}
}
mt.js.Tip.trackMenu = function(elt,onOut) {
	mt.js.Tip.init();
	var ftrack = null;
	var body = js.Lib.document.body;
	ftrack = function(evt) {
		if(mt.js.Tip.mousePos == null) return;
		var size = mt.js.Tip.elementSize(elt);
		if(mt.js.Tip.mousePos.x < size.x || mt.js.Tip.mousePos.y < size.y || mt.js.Tip.mousePos.x > size.x + size.width || mt.js.Tip.mousePos.y > size.y + size.height) {
			if(body.attachEvent) body.detachEvent("onmousemove",ftrack); else body.removeEventListener("mousemove",ftrack,false);
			onOut();
		}
	};
	if(body.attachEvent) body.attachEvent("onmousemove",ftrack); else body.addEventListener("mousemove",ftrack,false);
}
mt.js.Tip.init = function() {
	if(mt.js.Tip.initialized) return;
	if(document.body != null) {
		mt.js.Tip.initialized = true;
		document.body.onmousemove = mt.js.Tip.onMouseMove;
	}
}
var js = js || {}
mt.js.Timer = $hxClasses["mt.js.Timer"] = function(now,end,start) {
	this.t = now.getTime();
	this.start = start == null?now:start;
	this.end = end;
	if(mt.js.Timer.timer == null) {
		mt.js.Timer.timer = new haxe.Timer(1000);
		mt.js.Timer.timer.run = function() {
			var _g = 0, _g1 = mt.js.Timer.timers;
			while(_g < _g1.length) {
				var t = _g1[_g];
				++_g;
				t.update();
			}
		};
	}
	mt.js.Timer.timers.push(this);
};
mt.js.Timer.__name__ = ["mt","js","Timer"];
mt.js.Timer.timer = null;
mt.js.Timer.alloc = function(now,end,prec,div) {
	if(div == null) {
		div = "timer_" + mt.js.Timer.timers.length;
		js.Lib.document.write("<div id=\"" + div + "\" class=\"timer\"></div>");
	}
	var t = new mt.js.Timer(HxOverrides.strDate(now),HxOverrides.strDate(end));
	t.textDiv = { id : div, prec : prec};
	t.update();
	return t;
}
mt.js.Timer.prototype = {
	onUpdate: function() {
	}
	,onReady: function() {
		if(this.rem.time < -2) {
			js.Lib.window.location = js.Lib.window.location;
			this.onReady = function() {
			};
		}
	}
	,update: function() {
		this.t += 1000;
		var remt = (this.end.getTime() - this.t) / 1000;
		var rt = remt < 0?0:remt;
		this.rem = { days : rt / 86400 | 0, hours : (rt / 3600 | 0) % 24, minutes : (rt / 60 | 0) % 60, seconds : rt % 60 | 0, time : remt};
		var et = this.end.getTime();
		var st = this.start.getTime();
		this.progress = this.t >= et?1:(this.t - st) / (et - st);
		if(this.textDiv != null) {
			var div = js.Lib.document.getElementById(this.textDiv.id);
			if(div != null) div.innerHTML = this.buildText();
		}
		if(this.progressDiv != null) {
			var div = js.Lib.document.getElementById(this.progressDiv.id);
			if(div != null) {
				var w = this.progressDiv.width * this.progress | 0;
				div.style.width = w + "px";
			}
		}
		if(remt <= 0) this.onReady();
		this.onUpdate();
	}
	,buildText: function() {
		var str = "";
		var prec = this.textDiv.prec;
		var force = false;
		if(prec < 1) {
			var sep = this.rem.seconds % 2 == 0?":":"<span style=\"opacity : 0\">:</span>";
			if(this.rem.hours > 0) {
				var str1 = this.rem.hours + sep;
				if(this.rem.minutes < 10) str1 += "0";
				return str1 + this.rem.minutes;
			}
			var str1 = this.rem.minutes + sep;
			if(this.rem.seconds < 10) str1 += "0";
			return str1 + this.rem.seconds;
		}
		if(this.rem.days > 0) {
			str += this.rem.days + mt.js.Timer.TIMES.charAt(0) + " ";
			force = true;
			if(--prec == 0) return str;
		}
		if(force || this.rem.hours > 0) {
			str += this.rem.hours + mt.js.Timer.TIMES.charAt(1) + " ";
			force = true;
			if(--prec == 0) return str;
		}
		if(force || this.rem.minutes > 0) {
			if(force && this.rem.minutes < 10) str += "0";
			str += this.rem.minutes + mt.js.Timer.TIMES.charAt(2) + " ";
			force = true;
			if(--prec == 0) return str;
		}
		if(force && this.rem.seconds < 10) str += "0";
		str += this.rem.seconds + mt.js.Timer.TIMES.charAt(3) + " ";
		return str;
	}
	,stop: function() {
		HxOverrides.remove(mt.js.Timer.timers,this);
	}
	,progressDiv: null
	,textDiv: null
	,rem: null
	,progress: null
	,end: null
	,start: null
	,t: null
	,__class__: mt.js.Timer
}
js.App = $hxClasses["js.App"] = function() {
	this.adIndex = 0;
	this.adLock = 0;
	this.oldTips = new Hash();
};
js.App.__name__ = ["js","App"];
js.App.j = function(v) {
	return new js.JQuery(v);
}
js.App.main = function() {
	_ = new js.App();
}
js.App.prototype = {
	showDetails: function(id) {
		if(this.detailsCache == null) this.detailsCache = new IntHash();
		this.detailsCurrent = id;
		var data = this.detailsCache.get(id);
		if(data != null) {
			new js.JQuery("#trhome").html(data);
			return;
		}
		var me = this;
		var h = new haxe.Http("/details");
		h.setParameter("t",Std.string(id));
		h.onData = function(data1) {
			if(HxOverrides.substr(data1,0,5) == "<!DOC") return;
			me.detailsCache.set(id,data1);
			if(me.detailsCurrent == id) me.showDetails(id);
		};
		h.request(false);
	}
	,detailsCurrent: null
	,detailsCache: null
	,swfAction: function(swf,name) {
		var fobj = window.document[swf];
		if(fobj == null) fobj = window.document.getElementById(swf);
		if(fobj == null) throw "Could not find flash object '" + swf + "'";
		fobj[name]();
	}
	,hideNotif: function() {
		new js.JQuery("body").removeClass("hideSwf");
		new js.JQuery("#notification").remove();
		return false;
	}
	,deltaMoney: function(m) {
		var t = new haxe.Timer(30);
		t.run = function() {
			if(m <= 0) {
				t.stop();
				return;
			}
			var t1 = new js.JQuery("#targetMoney");
			t1.html(Std.string(Std.parseInt(t1.html()) + 1));
			m--;
		};
	}
	,adCycleDone: function(seed,down) {
		this.adLock--;
		if(this.adLock > 0) {
			this.adLock = 0;
			this.adCycle(seed,down);
		}
	}
	,adCycle: function(seed,down) {
		this.adLock++;
		if(this.adLock > 1) return;
		var me = this;
		if(down) this.adIndex--;
		var h = new haxe.Http("/ads");
		h.setParameter("seed",seed);
		h.setParameter("index",Std.string(this.adIndex));
		if(!down) this.adIndex++;
		h.onData = function(data) {
			if(StringTools.startsWith(data,"<!DOC")) js.Lib.document.write(data); else {
				new js.JQuery("#xds").html(data);
				if(!down) new js.JQuery("#xds ul").animate({ marginLeft : -232},1000,(function(f,a1,a2) {
					return function() {
						return f(a1,a2);
					};
				})($bind(me,me.adCycleDone),seed,down)); else {
					new js.JQuery("#xds ul").css("margin-left","-232px");
					new js.JQuery("#xds ul").animate({ marginLeft : 0},1000,(function(f1,a11,a21) {
						return function() {
							return f1(a11,a21);
						};
					})($bind(me,me.adCycleDone),seed,down));
				}
			}
		};
		h.request(false);
	}
	,startRecruit: function() {
		var h = new haxe.Http("/recruit");
		h.onData = function(data) {
			if(StringTools.startsWith(data,"<!DOC")) js.Lib.document.write(data); else {
				new js.JQuery("#recruit").html(data);
				new js.JQuery("body").addClass("hideSwf");
			}
		};
		h.request(false);
	}
	,initCreate: function(host) {
		var name = new js.JQuery("input#name");
		var valid = new js.JQuery("input#submit");
		var chk = new js.JQuery(".host");
		var cache = new Hash();
		name.focus();
		var timer = new haxe.Timer(30);
		var current = "";
		var wait = 0;
		var errors = 1;
		this.onTrooperSelected = function() {
			current = null;
		};
		timer.run = function() {
			var t = Common.host(name.val());
			if(t == current) {
				wait++;
				if(wait == 30 && !cache.exists(t)) {
					if(!Common.hostValid(t)) {
						cache.set(t,false);
						current = null;
						return;
					}
					cache.set(t,null);
					var h = new haxe.Http("/check");
					h.setParameter("name",t);
					h.onData = function(r) {
						var v;
						switch(r) {
						case "free":
							v = true;
							break;
						case "used":
							v = false;
							break;
						default:
							cache.remove(t);
							errors *= 2;
							wait = -errors;
							return;
						}
						cache.set(t,v);
						if(t != current) return;
						current = null;
					};
					h.request(false);
				}
				return;
			}
			current = t;
			chk.removeClass("free").removeClass("used");
			valid.attr("disabled","disabled");
			valid.addClass("button_off");
			if(cache.exists(t)) {
				wait = 1000;
				var v = cache.get(t);
				if(v != null) chk.addClass(v?"free":"used");
				if(v && new js.JQuery("#tselect").val() != "") {
					valid.removeClass("button_off");
					valid.removeAttr("disabled");
				}
			} else wait = 0;
			if(t == "") chk.hide(); else {
				chk.show();
				new js.JQuery(".hostname").text("http://" + t + "." + host);
			}
		};
	}
	,selectTrooper: function(tid,tval) {
		new js.JQuery("#tselect").val("" + tval);
		new js.JQuery(".trSelected").removeClass("trSelected");
		new js.JQuery("#tr_" + tid).addClass("trSelected");
		this.onTrooperSelected();
	}
	,resetTip: function(t) {
		var v = this.oldTips.get(t);
		var e = js.Lib.document.getElementById(t);
		e.innerHTML = v;
		e.parentNode.style.display = v == ""?"none":"";
	}
	,showTip: function(t,v) {
		var e = js.Lib.document.getElementById(t);
		if(!this.oldTips.exists(t)) this.oldTips.set(t,e.innerHTML);
		e.innerHTML = v;
		e.parentNode.style.display = "";
	}
	,toggle: function(id) {
		var elt = js.Lib.document.getElementById(id);
		elt.style.display = elt.style.display == "none"?"":"none";
		return false;
	}
	,oldTips: null
	,onTrooperSelected: null
	,adLock: null
	,adIndex: null
	,__class__: js.App
}
js.Boot = $hxClasses["js.Boot"] = function() { }
js.Boot.__name__ = ["js","Boot"];
js.Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
}
js.Boot.__trace = function(v,i) {
	var msg = i != null?i.fileName + ":" + i.lineNumber + ": ":"";
	msg += js.Boot.__string_rec(v,"");
	var d;
	if(typeof(document) != "undefined" && (d = document.getElementById("haxe:trace")) != null) d.innerHTML += js.Boot.__unhtml(msg) + "<br/>"; else if(typeof(console) != "undefined" && console.log != null) console.log(msg);
}
js.Boot.__clear_trace = function() {
	var d = document.getElementById("haxe:trace");
	if(d != null) d.innerHTML = "";
}
js.Boot.isClass = function(o) {
	return o.__name__;
}
js.Boot.isEnum = function(e) {
	return e.__ename__;
}
js.Boot.getClass = function(o) {
	return o.__class__;
}
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2, _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i;
			var str = "[";
			s += "\t";
			var _g = 0;
			while(_g < l) {
				var i1 = _g++;
				str += (i1 > 0?",":"") + js.Boot.__string_rec(o[i1],s);
			}
			str += "]";
			return str;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) { ;
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
}
js.Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0, _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js.Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js.Boot.__interfLoop(cc.__super__,cl);
}
js.Boot.__instanceof = function(o,cl) {
	try {
		if(o instanceof cl) {
			if(cl == Array) return o.__enum__ == null;
			return true;
		}
		if(js.Boot.__interfLoop(o.__class__,cl)) return true;
	} catch( e ) {
		if(cl == null) return false;
	}
	switch(cl) {
	case Int:
		return Math.ceil(o%2147483648.0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return o === true || o === false;
	case String:
		return typeof(o) == "string";
	case Dynamic:
		return true;
	default:
		if(o == null) return false;
		if(cl == Class && o.__name__ != null) return true; else null;
		if(cl == Enum && o.__ename__ != null) return true; else null;
		return o.__enum__ == cl;
	}
}
js.Boot.__cast = function(o,t) {
	if(js.Boot.__instanceof(o,t)) return o; else throw "Cannot cast " + Std.string(o) + " to " + Std.string(t);
}
js.Lib = $hxClasses["js.Lib"] = function() { }
js.Lib.__name__ = ["js","Lib"];
js.Lib.document = null;
js.Lib.window = null;
js.Lib.debug = function() {
	debugger;
}
js.Lib.alert = function(v) {
	alert(js.Boot.__string_rec(v,""));
}
js.Lib.eval = function(code) {
	return eval(code);
}
js.Lib.setErrorHandler = function(f) {
	js.Lib.onerror = f;
}
mt.ArrayStd = $hxClasses["mt.ArrayStd"] = function() { }
mt.ArrayStd.__name__ = ["mt","ArrayStd"];
mt.ArrayStd.size = function(ar) {
	return ar.length;
}
mt.ArrayStd.first = function(ar) {
	return ar[0];
}
mt.ArrayStd.last = function(ar) {
	return ar[ar.length - 1];
}
mt.ArrayStd.clear = function(ar) {
	ar.splice(0,ar.length);
	return ar;
}
mt.ArrayStd.set = function(ar,index,v) {
	ar[index] = v;
	return ar;
}
mt.ArrayStd.at = function(ar,index) {
	return ar[index];
}
mt.ArrayStd.indexOf = function(ar,elt) {
	var id = -1, i = -1;
	var _g = 0;
	while(_g < ar.length) {
		var e = ar[_g];
		++_g;
		++i;
		if(e == elt) {
			id = i;
			break;
		}
	}
	return id;
}
mt.ArrayStd.addFirst = function(ar,e) {
	ar.unshift(e);
	return ar;
}
mt.ArrayStd.addLast = function(ar,e) {
	ar.push(e);
	return ar;
}
mt.ArrayStd.removeFirst = function(ar) {
	return ar.shift();
}
mt.ArrayStd.removeLast = function(ar) {
	return ar.pop();
}
mt.ArrayStd.map = function(ar,f) {
	var output = [];
	var _g = 0;
	while(_g < ar.length) {
		var e = ar[_g];
		++_g;
		output.push(f(e));
	}
	return output;
}
mt.ArrayStd.stripNull = function(ar) {
	while(HxOverrides.remove(ar,null)) {
	}
	return ar;
}
mt.ArrayStd.flatten = function(ar) {
	var out = new Array();
	var _g1 = 0, _g = ar.length;
	while(_g1 < _g) {
		var i = _g1++;
		var $it0 = $iterator(ar[i])();
		while( $it0.hasNext() ) {
			var x = $it0.next();
			out.push(x);
			out;
		}
		out;
	}
	return out;
}
mt.ArrayStd.append = function(ar,it) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		ar.push(x);
		ar;
	}
	return ar;
}
mt.ArrayStd.prepend = function(ar,it) {
	var a = Lambda.array(it);
	a.reverse();
	var _g = 0;
	while(_g < a.length) {
		var x = a[_g];
		++_g;
		ar.unshift(x);
		ar;
	}
	return ar;
}
mt.ArrayStd.shuffle = function(ar,rand) {
	var rnd = rand != null?rand:Std.random;
	var size = ar.length;
	var _g1 = 0, _g = size << 1;
	while(_g1 < _g) {
		var i = _g1++;
		var id0 = rnd(size), id1 = rnd(size);
		var tmp = ar[id0];
		ar[id0] = ar[id1];
		ar[id1] = tmp;
	}
	return ar;
}
mt.ArrayStd.getRandom = function(ar,rnd) {
	var random = rnd != null?rnd:Std.random;
	var id = random(ar.length);
	return ar[id];
}
mt.ArrayStd.usort = function(t,f) {
	var a = t, i = 0, l = t.length;
	while(i < l) {
		var swap = false;
		var j = 0, max = l - i - 1;
		while(j < max) {
			if(f(a[j],a[j + 1]) > 0) {
				var tmp = a[j + 1];
				a[j + 1] = a[j];
				a[j] = tmp;
				swap = true;
			}
			j += 1;
		}
		if(!swap) break;
		i += 1;
	}
	return a;
}
mt.ListStd = $hxClasses["mt.ListStd"] = function() { }
mt.ListStd.__name__ = ["mt","ListStd"];
mt.ListStd.size = function(l) {
	return l.length;
}
mt.ListStd.at = function(l,index) {
	var ite = l.iterator();
	while(--index > -1) ite.next();
	return ite.next();
}
mt.ListStd.indexOf = function(l,elt) {
	var id = -1, i = -1;
	var $it0 = l.iterator();
	while( $it0.hasNext() ) {
		var e = $it0.next();
		++i;
		if(e == elt) {
			id = i;
			break;
		}
	}
	return id;
}
mt.ListStd.addFirst = function(l,e) {
	l.push(e);
	return l;
}
mt.ListStd.addLast = function(l,e) {
	l.add(e);
	return l;
}
mt.ListStd.removeFirst = function(l) {
	return l.pop();
}
mt.ListStd.removeLast = function(l) {
	var cpy = Lambda.list(l);
	var ite = cpy.iterator();
	var last = l.last();
	l.clear();
	var _g1 = 0, _g = cpy.length - 1;
	while(_g1 < _g) {
		var i = _g1++;
		l.add(ite.next());
	}
	return last;
}
mt.ListStd.copy = function(l) {
	return Lambda.list(l);
}
mt.ListStd.flatten = function(l) {
	var out = new List();
	var _g1 = 0, _g = l.length;
	while(_g1 < _g) {
		var i = _g1++;
		var $it0 = $iterator(mt.ListStd.at(l,i))();
		while( $it0.hasNext() ) {
			var x = $it0.next();
			out.add(x);
		}
		out;
	}
	return out;
}
mt.ListStd.append = function(l,it) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		l.add(x);
	}
	return l;
}
mt.ListStd.prepend = function(l,it) {
	var a = Lambda.array(it);
	a.reverse();
	var _g = 0;
	while(_g < a.length) {
		var x = a[_g];
		++_g;
		l.push(x);
		l;
	}
	return l;
}
mt.ListStd.reverse = function(l) {
	var cpy = [];
	while(l.length > 0) {
		cpy.unshift(l.pop());
		cpy;
	}
	while(cpy.length > 0) {
		l.push(cpy.pop());
		l;
	}
	return l;
}
mt.ListStd.shuffle = function(l,rand) {
	var ar = Lambda.array(l);
	mt.ArrayStd.shuffle(ar,rand);
	l.clear();
	var _g1 = 0, _g = ar.length;
	while(_g1 < _g) {
		var i = _g1++;
		l.add(ar[i]);
		l;
	}
	ar = null;
	return l;
}
mt.ListStd.slice = function(l,pos,end) {
	var out = new List();
	if(end == null) end = l.length;
	var _g = pos;
	while(_g < end) {
		var i = _g++;
		out.add(mt.ListStd.at(l,i));
		out;
	}
	return out;
}
mt.ListStd.splice = function(l,pos,len) {
	var out = new List();
	var copy = Lambda.list(l);
	l.clear();
	var i = 0;
	var $it0 = copy.iterator();
	while( $it0.hasNext() ) {
		var e = $it0.next();
		if(i < pos) {
			l.add(e);
			l;
		} else if(i >= pos + len) {
			l.add(e);
			l;
		} else {
			out.add(e);
			out;
		}
		i++;
	}
	return out;
}
mt.ListStd.stripNull = function(l) {
	while(l.remove(null)) {
	}
	return l;
}
mt.ListStd.getRandom = function(l,rnd) {
	var random = rnd != null?rnd:Std.random;
	var id = random(l.length);
	return mt.ListStd.at(l,id);
}
mt.ListStd.usort = function(l,f) {
	var a = Lambda.array(l);
	a = mt.ArrayStd.usort(a,f);
	l.clear();
	var _g = 0;
	while(_g < a.length) {
		var e = a[_g];
		++_g;
		l.add(e);
		l;
	}
	return l;
}
if(!mt.db) mt.db = {}
mt.db.Phoneme = $hxClasses["mt.db.Phoneme"] = function() {
	if(mt.db.Phoneme.tables == null) mt.db.Phoneme.initTables();
};
mt.db.Phoneme.__name__ = ["mt","db","Phoneme"];
mt.db.Phoneme.tables = null;
mt.db.Phoneme.initTables = function() {
	mt.db.Phoneme.tables = [{ terminal : null, table : []}];
	mt.db.Phoneme.repl("EAU","O");
	mt.db.Phoneme.repl("AU","O");
	mt.db.Phoneme.repl("OU","U");
	mt.db.Phoneme.repl("EU","e");
	mt.db.Phoneme.repl("AI","e");
	mt.db.Phoneme.repl("ER","e");
	mt.db.Phoneme.repl("CH","sh");
	mt.db.Phoneme.repl("OE","e");
	mt.db.Phoneme.repl("PH","F");
	mt.db.Phoneme.repl("H","");
	mt.db.Phoneme.repl("S$","$");
	mt.db.Phoneme.repl("T$","$");
	mt.db.Phoneme.repl("TS$","$");
	mt.db.Phoneme.repl("E$","$");
	mt.db.Phoneme.repl("ES$","$");
	mt.db.Phoneme.repl("P$","$");
	mt.db.Phoneme.repl("X$","$");
	mt.db.Phoneme.repl("ER$","e$");
	mt.db.Phoneme.repl("EE","e");
	mt.db.Phoneme.repl("AA","A");
	mt.db.Phoneme.repl("OO","O");
	mt.db.Phoneme.repl("UU","U");
	mt.db.Phoneme.repl("II","I");
	mt.db.Phoneme.repl("LL","L");
	mt.db.Phoneme.repl("TT","T");
	mt.db.Phoneme.repl("SS","S");
	mt.db.Phoneme.repl("NN","N");
	mt.db.Phoneme.repl("MM","N");
	mt.db.Phoneme.repl("RR","R");
	mt.db.Phoneme.repl("PP","P");
	mt.db.Phoneme.repl("FF","F");
	mt.db.Phoneme.repl("C","K");
	mt.db.Phoneme.repl("CE","SE");
	mt.db.Phoneme.repl("CS","X");
	mt.db.Phoneme.repl("CK","K");
	mt.db.Phoneme.repl("SK","K");
	mt.db.Phoneme.repl("QU","K");
	mt.db.Phoneme.repl("GU","G");
	mt.db.Phoneme.repl("GE","J");
	mt.db.Phoneme.repl("Y","I");
	mt.db.Phoneme.repl("Z","S");
	mt.db.Phoneme.repl("TIO","SIO");
	mt.db.Phoneme.repl("TIA","SIA");
	mt.db.Phoneme.repl("ERT","ert");
	mt.db.Phoneme.repl("EN","n");
	mt.db.Phoneme.repl("ON","n");
	mt.db.Phoneme.repl("ION","ioN");
	mt.db.Phoneme.repl("IN","n");
	mt.db.Phoneme.repl("INE","iNE");
	mt.db.Phoneme.repl("AIN","n");
	mt.db.Phoneme.repl("AN","n");
	mt.db.Phoneme.repl("AM","n");
	mt.db.Phoneme.repl("EM","n");
	mt.db.Phoneme.repl("OM","n");
	mt.db.Phoneme.repl("EMM","em");
	mt.db.Phoneme.repl("AMM","am");
	mt.db.Phoneme.repl("OMM","om");
}
mt.db.Phoneme.repl = function(a,b) {
	var state = 0;
	var _g1 = 0, _g = a.length;
	while(_g1 < _g) {
		var i = _g1++;
		var t = mt.db.Phoneme.tables[state].table;
		var c = HxOverrides.cca(a,i);
		state = t[c];
		if(state == null) {
			state = mt.db.Phoneme.tables.length;
			t[c] = state;
			mt.db.Phoneme.tables.push({ terminal : null, table : new Array()});
		}
	}
	var t = mt.db.Phoneme.tables[state];
	if(t.terminal != null) throw "Duplicate replace " + a;
	if(a.length < b.length) throw "Invalid replace " + a + ":" + b;
	t.terminal = haxe.io.Bytes.ofString(b);
}
mt.db.Phoneme.removeAccentsUTF8 = function(s) {
	var b = new StringBuf();
	var _g1 = 0, _g = s.length;
	while(_g1 < _g) {
		var i = _g1++;
		var c = HxOverrides.cca(s,i);
		switch(c) {
		case 233:case 232:case 234:case 235:
			b.b += "e";
			break;
		case 201:case 200:case 202:case 203:
			b.b += "E";
			break;
		case 224:case 226:case 228:case 225:
			b.b += "a";
			break;
		case 192:case 194:case 196:case 193:
			b.b += "A";
			break;
		case 249:case 251:case 252:case 250:
			b.b += "u";
			break;
		case 217:case 219:case 220:case 218:
			b.b += "U";
			break;
		case 238:case 239:case 237:
			b.b += "i";
			break;
		case 206:case 207:case 205:
			b.b += "I";
			break;
		case 244:case 243:case 246:case 245:
			b.b += "o";
			break;
		case 212:case 211:case 214:
			b.b += "O";
			break;
		case 230:case 198:
			b.b += "a";
			b.b += "e";
			break;
		case 339:case 338:
			b.b += "o";
			b.b += "e";
			break;
		case 231:
			b.b += "c";
			break;
		case 199:
			b.b += "C";
			break;
		case 241:
			b.b += "n";
			break;
		case 209:
			b.b += "N";
			break;
		default:
			b.b += String.fromCharCode(c);
		}
	}
	return b.b;
}
mt.db.Phoneme.levenshtein = function(a,b) {
	var d = [];
	var k = a.length + 1;
	var k2 = b.length + 1;
	var _g = 0;
	while(_g < k) {
		var i = _g++;
		d[i] = i;
	}
	var _g = 0;
	while(_g < k2) {
		var j = _g++;
		d[j * k] = j;
	}
	var _g = 1;
	while(_g < k) {
		var i = _g++;
		var _g1 = 1;
		while(_g1 < k2) {
			var j = _g1++;
			var v = a.cca(i - 1) == b.cca(j - 1)?0:1;
			var er = d[i - 1 + j * k] + 1;
			var ins = d[i + (j - 1) * k] + 1;
			var sub = d[i - 1 + (j - 1) * k] + v;
			d[i + j * k] = er < ins?er < sub?er:sub:ins < sub?ins:sub;
		}
	}
	return d[a.length + b.length * k];
}
mt.db.Phoneme.prototype = {
	make: function(s) {
		s = mt.db.Phoneme.removeAccentsUTF8(s);
		s = s.toUpperCase();
		var buf = new StringBuf();
		var b = haxe.io.Bytes.ofString("$" + new EReg("[^A-Z]+","g").split(s).join("$") + "$");
		var i = 0;
		var max = b.length;
		var tables = mt.db.Phoneme.tables;
		var t, state;
		var lastpos = 0, last, startpos;
		while(i < max) {
			last = null;
			startpos = i;
			t = tables[0];
			do {
				state = t.table[b.b[i]];
				if(state == null) break;
				t = tables[state];
				++i;
				if(t.terminal != null) {
					last = t.terminal;
					lastpos = i;
				}
			} while(i < max);
			if(last == null) {
				i = startpos;
				var c = b.b[i];
				if(c >= 97 && c <= 122) c += -32;
				buf.b += String.fromCharCode(c);
				i++;
			} else {
				var len = last.length;
				i = lastpos - len;
				b.blit(i,last,0,len);
			}
		}
		return buf.b;
	}
	,get: function(at) {
		return HxOverrides.cca(this.s,at);
	}
	,s: null
	,__class__: mt.db.Phoneme
}
function $iterator(o) { if( o instanceof Array ) return function() { return HxOverrides.iter(o); }; return typeof(o.iterator) == 'function' ? $bind(o,o.iterator) : o.iterator; };
var $_;
function $bind(o,m) { var f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; return f; };
if(Array.prototype.indexOf) HxOverrides.remove = function(a,o) {
	var i = a.indexOf(o);
	if(i == -1) return false;
	a.splice(i,1);
	return true;
}; else null;
if(String.prototype.cca == null) String.prototype.cca = String.prototype.charCodeAt;
Math.__name__ = ["Math"];
Math.NaN = Number.NaN;
Math.NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY;
Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
$hxClasses.Math = Math;
Math.isFinite = function(i) {
	return isFinite(i);
};
Math.isNaN = function(i) {
	return isNaN(i);
};
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = ["String"];
Array.prototype.__class__ = $hxClasses.Array = Array;
Array.__name__ = ["Array"];
Date.prototype.__class__ = $hxClasses.Date = Date;
Date.__name__ = ["Date"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = $hxClasses.Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
var Void = $hxClasses.Void = { __ename__ : ["Void"]};
mt.js.Tip.init();
var q = window.jQuery;
js.JQuery = q;
q.fn.iterator = function() {
	return { pos : 0, j : this, hasNext : function() {
		return this.pos < this.j.length;
	}, next : function() {
		return $(this.j[this.pos++]);
	}};
};
if(typeof document != "undefined") js.Lib.document = document;
if(typeof window != "undefined") {
	js.Lib.window = window;
	js.Lib.window.onerror = function(msg,url,line) {
		var f = js.Lib.onerror;
		if(f == null) return false;
		return f(msg,[url + ":" + line]);
	};
}
js.XMLHttpRequest = window.XMLHttpRequest?XMLHttpRequest:window.ActiveXObject?function() {
	try {
		return new ActiveXObject("Msxml2.XMLHTTP");
	} catch( e ) {
		try {
			return new ActiveXObject("Microsoft.XMLHTTP");
		} catch( e1 ) {
			throw "Unable to create XMLHttpRequest object.";
		}
	}
}:(function($this) {
	var $r;
	throw "Unable to create XMLHttpRequest object.";
	return $r;
}(this));
mt.js.Tip.xOffset = 3;
mt.js.Tip.yOffset = 22;
mt.js.Tip.defaultClass = "normalTip";
mt.js.Tip.tooltipId = "tooltip";
mt.js.Tip.tooltipContentId = "tooltipContent";
mt.js.Tip.minOffsetY = 23;
mt.js.Tip.tipZIndex = 10;
mt.js.Timer.timers = new Array();
mt.js.Timer.TIMES = "jhms";
js.App.ref = [mt.js.Tip,js.JQuery,mt.js.Timer,haxe.Md5];
js.Lib.onerror = null;
js.App.main();
