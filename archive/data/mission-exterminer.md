# Mission : Exterminer

L'armée doit exterminer un certain nombre de rats, nombre croissant à chaque nouvelle session terminée, jusqu'au point où il est techniquement impossible de les vaincre. Les rats ne s'engagent qu'en mélée, mais en quelques jours, ils sont capables de submerger les troupes avec un nombre impressionnant, nécessitant une approche spéciale pour réussir.

## Flashvar

### Messages de fin (FR) :

#### Victoire
 `&endMsg=F%C3%A9licitations%2C%20tu%20as%20remport%C3%A9%20la%20mission%20du%20jour%20%21%20Ton%20arm%C3%A9e%20gagne%20%2B4%20%5BT%5D%20et%20une%20nouvelle%20mission%20sera%20disponible%20bient%C3%B4t.`

> Félicitations, tu as remporté la mission du jour ! Ton armée gagne +4 [T] et une nouvelle mission sera disponible bientôt.

#### Victoire (replay)

`endMsg=Tu%20as%20remport%C3%A9%20ce%20combat%20%21`

> Tu as remporté ce combat !

#### Défaite (et replay)

`&endMsg=Ce%20combat%20s%27est%20sold%C3%A9%20par%20un%20%C3%A9chec%20%21`

> Ce combat s'est soldé par un échec !

#### Défaite (ultime tentative)

`&endMsg=Cette%20mission%20n%27a%20pas%20pu%20%C3%AAtre%20termin%C3%A9e.%20Une%20nouvelle%20mission%20sera%20disponible%20bient%C3%B4t%20%21`

> Cette mission n'a pas pu être terminée. Une nouvelle mission sera disponible bientôt !

### Réfs. d'intégration du client swf

- id: `swf_battle`
- height: `440`
- width: `720`

### Remarques

- `flashvars.mode[2].bg` : `{"id": ["BG_ATTIC", 1], "gfx": "bg/attic.jpg"}`
- `flashvars.mode[1].id` : *(Int)* (?)
- `flashvars.mode[2].armies[0]` : Troupe constituée de l'armée principale.
- `flashvars.mode[2].armies[1]` : Troupe constituée de rats
- `flashvars.mode[2].armies[0].`*__UP2* : *(Array)* Priorités d'apparition des unités (obfu: `K)\x16\n\x02`)

### Exemple

<details><summary>Exemple</summary>

```javascript
{
  "mode": [
    "eU\\u\u0001",
    0,
    {
      "bg": {
        "id": [
          "BG_ATTIC",
          1
        ],
        "gfx": "bg/attic.jpg"
      },
      "id": 12429808,
      "armies": [
        {
          "troopers": [
            {
              "name": "Jomille",
              "pref": {
                "moveSystem": [
                  "J\u0016$2",
                  2
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": [
                  "o\u0012Q\u0007\u0003",
                  27
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "SPY",
                    5
                  ]
                ],
                "targetSystem": [
                  "%i\u001e2\u0001",
                  3
                ]
              },
              "seed": 5433205,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                1,
                0,
                0,
                0
              ],
              "id": 0,
              "force": []
            },
            {
              "name": "T-Bombewise",
              "pref": {
                "moveSystem": [
                  "\n\u0001O\u0002",
                  0
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": [
                  "Xp?\u0016\u0003",
                  7
                ],
                "targetType": [
                  "RO)%",
                  0,
                  [
                    "\u001fj\u00180\u0001",
                    6
                  ]
                ],
                "targetSystem": [
                  "%i\u001e2\u0001",
                  3
                ]
              },
              "seed": 6255794,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                0,
                1,
                0,
                1
              ],
              "id": 1,
              "force": []
            },
            {
              "name": "hnyibh ky",
              "pref": {
                "moveSystem": [
                  "J\u0016$2",
                  2
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": [
                  "v*E2\u0003",
                  26
                ],
                "targetType": [
                  "3YX,\u0003",
                  0
                ],
                "targetSystem": [
                  "rf_U\u0003",
                  1
                ]
              },
              "seed": 7100836,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                1,
                1,
                0
              ],
              "id": 2,
              "force": []
            },
            {
              "name": "bruto",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": [
                  "AvZZ\u0003",
                  18
                ],
                "targetType": [
                  "3YX,\u0003",
                  0
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 4125669,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                0,
                0,
                0
              ],
              "id": 3,
              "force": []
            },
            {
              "name": "petit PD",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": [
                  "HEAD",
                  0
                ],
                "__CWeapon": null,
                "targetType": [
                  "3YX,\u0003",
                  0
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 12710383,
              "type": [
                "HUMAN",
                0
              ],
              "choices": [
                1,
                1,
                2
              ],
              "id": 4,
              "force": []
            }
          ],
          "__UP2": [
            0,
            1,
            2,
            3,
            4
          ],
          "faction": 4
        },
        {
          "c)4p\u0003": 0,
          "U,*~\u0001": null,
          "troopers": [
            {
              "name": "rat",
              "pref": {
                "moveSystem": [
                  "}C9g",
                  1
                ],
                "reloadSystem": [
                  "\u0015Z^\u001d\u0001",
                  2
                ],
                "leftOver": [],
                "__CBody": null,
                "__CWeapon": null,
                "targetType": [
                  "3YX,\u0003",
                  0
                ],
                "targetSystem": [
                  "0QCw\u0001",
                  0
                ]
              },
              "seed": 3610,
              "type": [
                "RAT",
                2
              ],
              "choices": [],
              "id": 5,
              "force": [
                [
                  "\u0010\nP\u001f\u0001",
                  117
                ],
                [
                  "~\u0013ds",
                  55
                ]
              ]
            }
          ],
          "__UP2": [],
          "faction": 0
        }
      ]
    },
    0
  ],
  "gfx": "http://data.minitroopers.fr/swf/army.swf?v=2"
}
```
</details>

<details><summary>Flashvar brut</summary>

`data=oy10%3A%253B%2501%250CZjy10%3AClientMode%3A0%3A2oy7%3A%252AM%2501oy5%3A7X%2501jy14%3ABackgroundType%3A1%3A0y8%3A%2524PS%2501y14%3Abg%252Fattic.jpggR3i12429808y11%3Aa%255De%251B%2502aoy13%3A%25601%251F%252A%2502aoy8%3A%25032%251A4y7%3AJomilley8%3AV%251BE%257Coy9%3A%251DlQK%2503jy10%3AMoveSystem%3A2%3A0y13%3A%2511%2521%250Ea%2501jy12%3AReloadSystem%3A2%3A0y11%3A%25290%2519i%2503ahy15%3A%251A%2529%2513%253D%2501jy7%3ABodyLoc%3A0%3A0y13%3A%250BR%2510%251B%2502jy6%3AWeapon%3A27%3A0y13%3A%2515S%2517%253F%2503jy10%3ATargetType%3A1%3A1jy5%3ASkill%3A5%3A0y6%3A%253BuXEjy12%3ATargetSystem%3A3%3A0gy6%3A%250CA2ci5433205y6%3AAn%25256jy11%3ATrooperType%3A0%3A0y8%3A%250Az%250Avai1i1zzzhR3zy13%3A%257D%2526%2516Y%2501ahgoR9y11%3AT-BombewiseR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ahR17jR18%3A0%3A0R19jR20%3A7%3A0R21jR22%3A1%3A1jR23%3A6%3A0R24jR25%3A3%3A0gR26i6255794R27jR28%3A0%3A0R29azzi1zi1hR3i1R30ahgoR9y11%3Ahnyibh%2520kyR11oR12jR13%3A2%3A0R14jR15%3A2%3A0R16ahR17jR18%3A0%3A0R19jR20%3A26%3A0R21jR22%3A0%3A0R24jR25%3A1%3A0gR26i7100836R27jR28%3A0%3A0R29azi1i1zhR3i2R30ahgoR9y5%3AbrutoR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR17jR18%3A0%3A0R19jR20%3A18%3A0R21jR22%3A0%3A0R24jR25%3A0%3A0gR26i4125669R27jR28%3A0%3A0R29azzzhR3i3R30ahgoR9y10%3Apetit%2520PDR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR17jR18%3A0%3A0R19nR21jR22%3A0%3A0R24jR25%3A0%3A0gR26i12710383R27jR28%3A0%3A0R29ai1i1i2hR3i4R30ahghy13%3AK%2529%2516%250A%2502azi1i2i3i4hy9%3AOPz%2516%2503i4goy9%3Ac%25294p%2503zR8aoR9y3%3AratR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR17nR19nR21jR22%3A0%3A0R24jR25%3A0%3A0gR26i3610R27jR28%3A2%3A0R29ahR3i5R30ajR23%3A117%3A0jR23%3A55%3A0hghR35ahR36zy13%3AU%252C%252A%257E%2501nghgzR5y58%3Ahttp%253A%252F%252Fdata.minitroopers.fr%252Fswf%252Farmy.swf%253Fv%253D2g&chk=c560eca4532ba5564192739620a5b9dc&imageurl=http://data.minitroopers.fr/img/&endMsg=F%C3%A9licitations%2C%20tu%20as%20remport%C3%A9%20la%20mission%20du%20jour%20%21%20Ton%20arm%C3%A9e%20gagne%20%2B4%20%5BT%5D%20et%20une%20nouvelle%20mission%20sera%20disponible%20bient%C3%B4t.`
</details>