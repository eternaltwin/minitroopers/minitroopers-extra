# Mission : Epique

L'armée assistée par les meilleures recrues, combat un assortiment de soldats (non-joueurs) qui sont généralement plus nombreux d'un cran et peuvent transporter plus de 3 pièces de petit équipement, mais manquent de niveaux après quelques vagues. Ce type de mission n'est pas proposé tant qu'un certain nombre de recrues n'est atteint.

## Flashvar

### Messages de fin (EN) :

#### Victoire

#### Victoire (replay)

`&endMsg=You%20have%20won%20the%20battle%21`

> You have won the battle!

#### Défaite (et replay)

#### Défaite (ultime tentative)

### Réfs. d'intégration du client swf

- id: `swf_battle`
- height: `440`
- width: `720`

### Remarques

- `flashvars.mode[2].bg` : `{"id": ["BG_ROAD", 3], "gfx": "bg/road.jpg"}`
- `flashvars.mode[1].id` : *(Int)* (?)
- `flashvars.mode[2].armies[0]` : Troupe constituée de l'armée principale, complétés par les recrues.
- `flashvars.mode[2].armies[1]` : *Inutilisé ?* Troupe adverse
- `flashvars.mode[2].armies[0].`*__UP2* : *(Array)* Priorités d'apparition des unités (obfu: `K)\x16\n\x02`)

### Exemple
<details><summary>Exemple</summary>

```javascript
[
  {
    "id": "swf_battle",
    "width": "720",
    "height": "440",
    "fv": {
      "mode": [
        "eU\\u\u0001",
        0,
        {
          "bg": {
            "id": [
              "BG_ROAD",
              3
            ],
            "gfx": "bg/road.jpg"
          },
          "id": 14983992,
          "armies": [
            {
              "troopers": [
                {
                  "name": "lalalatofudi",
                  "pref": {
                    "moveSystem": [
                      "J\u0016$2",
                      2
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [
                      [
                        "$__,\u0001",
                        125
                      ]
                    ],
                    "__CBody": [
                      "STOMACH",
                      3
                    ],
                    "__CWeapon": [
                      "#i6\u001b",
                      17
                    ],
                    "targetType": [
                      "RO)%",
                      0,
                      [
                        "MEDIC",
                        1
                      ]
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 5589344,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    1,
                    1,
                    1,
                    0,
                    1,
                    0,
                    1,
                    0,
                    1,
                    0
                  ],
                  "id": 0,
                  "force": []
                },
                {
                  "name": "Your a Bitch",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "HEAD",
                      0
                    ],
                    "__CWeapon": [
                      "o\u0012Q\u0007\u0003",
                      27
                    ],
                    "targetType": [
                      "RO)%",
                      0,
                      [
                        "SPY",
                        5
                      ]
                    ],
                    "targetSystem": [
                      "%i\u001e2\u0001",
                      3
                    ]
                  },
                  "seed": 16683160,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    0,
                    1,
                    0,
                    0,
                    0,
                    1,
                    0,
                    0
                  ],
                  "id": 1,
                  "force": []
                },
                {
                  "name": "lock account",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "HEAD",
                      0
                    ],
                    "__CWeapon": [
                      "Z\tvt\u0001",
                      2
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 3713712,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    1,
                    0,
                    1,
                    1,
                    1,
                    0,
                    0,
                    1
                  ],
                  "id": 2,
                  "force": []
                },
                {
                  "name": "SUCK IT",
                  "pref": {
                    "moveSystem": [
                      "\n\u0001O\u0002",
                      0
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "TORSO_LEFT",
                      1
                    ],
                    "__CWeapon": [
                      "(7*\\\u0001",
                      8
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "%i\u001e2\u0001",
                      3
                    ]
                  },
                  "seed": 13728955,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    0,
                    1,
                    0,
                    0,
                    1,
                    1,
                    0,
                    0
                  ],
                  "id": 3,
                  "force": []
                },
                {
                  "name": "Rogers",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [
                      [
                        "\u0016\rz\u0001\u0002",
                        57
                      ]
                    ],
                    "__CBody": null,
                    "__CWeapon": [
                      "SG\tY\u0003",
                      22
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 12753056,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    0,
                    1,
                    1,
                    0,
                    0,
                    0,
                    1,
                    1
                  ],
                  "id": 4,
                  "force": []
                },
                {
                  "name": "make a PW..",
                  "pref": {
                    "moveSystem": [
                      "\n\u0001O\u0002",
                      0
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "TORSO_LEFT",
                      1
                    ],
                    "__CWeapon": [
                      "\th/i",
                      9
                    ],
                    "targetType": [
                      "RO)%",
                      0,
                      [
                        "SPY",
                        5
                      ]
                    ],
                    "targetSystem": [
                      "%i\u001e2\u0001",
                      3
                    ]
                  },
                  "seed": 3363687,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    1,
                    0,
                    1,
                    0,
                    1,
                    0,
                    0,
                    0
                  ],
                  "id": 5,
                  "force": []
                },
                {
                  "name": "im gay",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "STOMACH",
                      3
                    ],
                    "__CWeapon": [
                      "AvZZ\u0003",
                      18
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "%i\u001e2\u0001",
                      3
                    ]
                  },
                  "seed": 5106883,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0,
                    1,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                  ],
                  "id": 6,
                  "force": []
                },
                {
                  "name": "Chri",
                  "pref": {
                    "moveSystem": [
                      "J\u0016$2",
                      2
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": null,
                    "__CWeapon": [
                      "#i6\u001b",
                      17
                    ],
                    "targetType": [
                      "RO)%",
                      0,
                      [
                        "SOLDAT",
                        0
                      ]
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 1529092,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0,
                    1,
                    0,
                    1,
                    1,
                    0,
                    0,
                    1
                  ],
                  "id": 7,
                  "force": []
                },
                {
                  "name": "Maxust-X",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "HEAD",
                      0
                    ],
                    "__CWeapon": [
                      "'z\u0006\u0010\u0003",
                      24
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 12069782,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0,
                    1,
                    0
                  ],
                  "id": 8,
                  "force": []
                },
                {
                  "name": "Chriwe",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": null,
                    "__CWeapon": [
                      ")L0t\u0002",
                      12
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 7847188,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0,
                    0,
                    0
                  ],
                  "id": 9,
                  "force": []
                },
                {
                  "name": "Maxowney",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": null,
                    "__CWeapon": [
                      "|y}L\u0003",
                      20
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "%i\u001e2\u0001",
                      3
                    ]
                  },
                  "seed": 9091145,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    1
                  ],
                  "id": 10,
                  "force": []
                },
                {
                  "name": "Jazoch Brute",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "TORSO_LEFT",
                      1
                    ],
                    "__CWeapon": [
                      "|y}L\u0003",
                      20
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "%i\u001e2\u0001",
                      3
                    ]
                  },
                  "seed": 4168823,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0,
                    0
                  ],
                  "id": 11,
                  "force": []
                },
                {
                  "name": "Georgey Dick",
                  "pref": null,
                  "seed": 5066750,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    1
                  ],
                  "id": 12,
                  "force": []
                },
                {
                  "name": "Georgey Dick",
                  "pref": null,
                  "seed": 5066750,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    1
                  ],
                  "id": 13,
                  "force": []
                },
                {
                  "name": "Crakenser",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "HEAD",
                      0
                    ],
                    "__CWeapon": null,
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "%i\u001e2\u0001",
                      3
                    ]
                  },
                  "seed": 11992421,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1,
                    0
                  ],
                  "id": 14,
                  "force": []
                },
                {
                  "name": "Huffewis",
                  "pref": null,
                  "seed": 15332096,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0
                  ],
                  "id": 15,
                  "force": []
                },
                {
                  "name": "Kevoff",
                  "pref": null,
                  "seed": 14090579,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1
                  ],
                  "id": 16,
                  "force": []
                },
                {
                  "name": "Huffips",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": null,
                    "__CWeapon": [
                      "||e\u0007\u0003",
                      29
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 8762231,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0
                  ],
                  "id": 17,
                  "force": []
                },
                {
                  "name": "Sgt.Maxikins",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": [
                      "TORSO_LEFT",
                      1
                    ],
                    "__CWeapon": null,
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 5810860,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    0
                  ],
                  "id": 18,
                  "force": []
                },
                {
                  "name": "Lukando",
                  "pref": {
                    "moveSystem": [
                      "}C9g",
                      1
                    ],
                    "reloadSystem": [
                      "\u0015Z^\u001d\u0001",
                      2
                    ],
                    "leftOver": [],
                    "__CBody": null,
                    "__CWeapon": [
                      "||e\u0007\u0003",
                      29
                    ],
                    "targetType": [
                      "3YX,\u0003",
                      0
                    ],
                    "targetSystem": [
                      "0QCw\u0001",
                      0
                    ]
                  },
                  "seed": 14432959,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [
                    1
                  ],
                  "id": 19,
                  "force": []
                },
                {
                  "name": "Don Melones",
                  "pref": null,
                  "seed": 11928406,
                  "type": [
                    "HUMAN",
                    0
                  ],
                  "choices": [],
                  "id": 20,
                  "force": []
                }
              ],
              "__UP2": [
                0,
                1,
                2,
                3,
                4,
                5,
                6
              ],
              "faction": 1
            },
            {
              "troopers": [],
              "__UP2": [],
              "faction": 0
            }
          ]
        },
        0
      ],
      "gfx": "http://data.minitroopers.com/swf/army.swf?v=1"
    },
  }
]
```
</details>

<details><summary>Flashvar brut</summary>

`data=oy10%3A%253B%2501%250CZjy10%3AClientMode%3A0%3A2oy7%3A%252AM%2501oy5%3A7X%2501jy14%3ABackgroundType%3A3%3A0y8%3A%2524PS%2501y13%3Abg%252Froad.jpggR3i14983992y11%3Aa%255De%251B%2502aoy13%3A%25601%251F%252A%2502aoy8%3A%25032%251A4y12%3Alalalatofudiy8%3AV%251BE%257Coy9%3A%251DlQK%2503jy10%3AMoveSystem%3A2%3A0y13%3A%2511%2521%250Ea%2501jy12%3AReloadSystem%3A2%3A0y11%3A%25290%2519i%2503ajy5%3ASkill%3A125%3A0hy15%3A%251A%2529%2513%253D%2501jy7%3ABodyLoc%3A3%3A0y13%3A%250BR%2510%251B%2502jy6%3AWeapon%3A17%3A0y13%3A%2515S%2517%253F%2503jy10%3ATargetType%3A1%3A1jR17%3A1%3A0y6%3A%253BuXEjy12%3ATargetSystem%3A0%3A0gy6%3A%250CA2ci5589344y6%3AAn%25256jy11%3ATrooperType%3A0%3A0y8%3A%250Az%250Avai1i1i1i1zi1zi1zi1zhR3zy13%3A%257D%2526%2516Y%2501ahgoR9y16%3AYour%2520a%2520BitchR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18jR19%3A0%3A0R20jR21%3A27%3A0R22jR23%3A1%3A1jR17%3A5%3A0R24jR25%3A3%3A0gR26i16683160R27jR28%3A0%3A0R29ai1zi1zzzi1zzhR3i1R30ahgoR9y14%3Alock%2520accountR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18jR19%3A0%3A0R20jR21%3A2%3A0R22jR23%3A0%3A0R24jR25%3A0%3A0gR26i3713712R27jR28%3A0%3A0R29ai1i1zi1i1i1zzi1hR3i2R30ahgoR9y9%3ASUCK%2520ITR11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ahR18jR19%3A1%3A0R20jR21%3A8%3A0R22jR23%3A0%3A0R24jR25%3A3%3A0gR26i13728955R27jR28%3A0%3A0R29ai1zi1zzi1i1zzhR3i3R30ahgoR9y6%3ARogersR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ajR17%3A57%3A0hR18nR20jR21%3A22%3A0R22jR23%3A0%3A0R24jR25%3A0%3A0gR26i12753056R27jR28%3A0%3A0R29ai1zi1i1zzzi1i1hR3i4R30ahgoR9y15%3Amake%2520a%2520PW..R11oR12jR13%3A0%3A0R14jR15%3A2%3A0R16ahR18jR19%3A1%3A0R20jR21%3A9%3A0R22jR23%3A1%3A1jR17%3A5%3A0R24jR25%3A3%3A0gR26i3363687R27jR28%3A0%3A0R29ai1i1zi1zi1zzzhR3i5R30ahgoR9y8%3Aim%2520gayR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18jR19%3A3%3A0R20jR21%3A18%3A0R22jR23%3A0%3A0R24jR25%3A3%3A0gR26i5106883R27jR28%3A0%3A0R29azi1zzzzzzhR3i6R30ahgoR9y4%3AChriR11oR12jR13%3A2%3A0R14jR15%3A2%3A0R16ahR18nR20jR21%3A17%3A0R22jR23%3A1%3A1jR17%3A0%3A0R24jR25%3A0%3A0gR26i1529092R27jR28%3A0%3A0R29azi1zi1i1zzi1hR3i7R30ahgoR9y8%3AMaxust-XR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18jR19%3A0%3A0R20jR21%3A24%3A0R22jR23%3A0%3A0R24jR25%3A0%3A0gR26i12069782R27jR28%3A0%3A0R29azi1zhR3i8R30ahgoR9y6%3AChriweR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18nR20jR21%3A12%3A0R22jR23%3A0%3A0R24jR25%3A0%3A0gR26i7847188R27jR28%3A0%3A0R29azzzhR3i9R30ahgoR9y8%3AMaxowneyR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18nR20jR21%3A20%3A0R22jR23%3A0%3A0R24jR25%3A3%3A0gR26i9091145R27jR28%3A0%3A0R29ai1i1hR3i10R30ahgoR9y14%3AJazoch%2520BruteR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18jR19%3A1%3A0R20jR21%3A20%3A0R22jR23%3A0%3A0R24jR25%3A3%3A0gR26i4168823R27jR28%3A0%3A0R29azzhR3i11R30ahgoR9y14%3AGeorgey%2520DickR11nR26i5066750R27jR28%3A0%3A0R29ai1i1hR3i12R30ahgoR9R42R11nR26i5066750R27jR28%3A0%3A0R29ai1i1hR3i13R30ahgoR9y9%3ACrakenserR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18jR19%3A0%3A0R20nR22jR23%3A0%3A0R24jR25%3A3%3A0gR26i11992421R27jR28%3A0%3A0R29ai1zhR3i14R30ahgoR9y8%3AHuffewisR11nR26i15332096R27jR28%3A0%3A0R29azhR3i15R30ahgoR9y6%3AKevoffR11nR26i14090579R27jR28%3A0%3A0R29ai1hR3i16R30ahgoR9y7%3AHuffipsR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18nR20jR21%3A29%3A0R22jR23%3A0%3A0R24jR25%3A0%3A0gR26i8762231R27jR28%3A0%3A0R29azhR3i17R30ahgoR9y12%3ASgt.MaxikinsR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18jR19%3A1%3A0R20nR22jR23%3A0%3A0R24jR25%3A0%3A0gR26i5810860R27jR28%3A0%3A0R29azhR3i18R30ahgoR9y7%3ALukandoR11oR12jR13%3A1%3A0R14jR15%3A2%3A0R16ahR18nR20jR21%3A29%3A0R22jR23%3A0%3A0R24jR25%3A0%3A0gR26i14432959R27jR28%3A0%3A0R29ai1hR3i19R30ahgoR9y13%3ADon%2520MelonesR11nR26i11928406R27jR28%3A0%3A0R29ahR3i20R30ahghy13%3AK%2529%2516%250A%2502azi1i2i3i4i5i6hy9%3AOPz%2516%2503i1goR8ahR50ahR51zghgzR5y59%3Ahttp%253A%252F%252Fdata.minitroopers.com%252Fswf%252Farmy.swf%253Fv%253D1g&amp;chk=9dafc45ec112daa81eb25d45aa3c3275&amp;imageurl=http://data.minitroopers.com/img/&amp;endMsg=You%20have%20won%20the%20battle%21">`
</details>