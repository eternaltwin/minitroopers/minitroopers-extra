# Weapons

### Spritesheet

![Spritesheet](../gfx/weapons/weapons_icons.png)

### icons

V | skill | icon | mini | nom | Clip | Utilisée
:-:|:-:|:-:|:-:|---|---|---
| V |  8 | ![skill 8](../gfx/skills/skill_8.png)  | ![handgun_1](../gfx/weapons/handgun_1.png)        | Pistolet            | `(  0, 0,34,18)` | Oui
| V | 12 | ![skill 12](../gfx/skills/skill_12.png)  | ![handgun_1](../gfx/weapons/handgun_1.png)      | Double pistolet     | `(  0, 0,34,18)` | Oui
| V |  9 | ![skill 9](../gfx/skills/skill_9.png) | ![handgun_2](../gfx/weapons/handgun_2.png)         | Revolver            | `(  0,18,34,18)` | Oui
| V | 11 | ![skill 11](../gfx/skills/skill_11.png) | ![handgun_3](../gfx/weapons/handgun_3.png)       | Beretta             | `(  0,36,34,18)` | Oui
|  |  | ![skill ](../gfx/skills/skill_.png) | ![handgun_4](../gfx/weapons/handgun_4.png)              | *handgun_4*         | `(  0,54,34,18)` | *Non*
|  |  | ![skill ](../gfx/skills/skill_.png) | ![handgun_5](../gfx/weapons/handgun_5.png)              | *handgun_5*         | `(  0,72,34,18)` | *Non*
| V | 10 | ![skill 10](../gfx/skills/skill_10.png) | ![handgun_6](../gfx/weapons/handgun_6.png)       | Desert Eagle        | `(  0,90,34,18)` | Oui
| V | 20 | ![skill 20](../gfx/skills/skill_20.png) | ![rifle_1](../gfx/weapons/rifle_1.png)           | AK47                | `( 34, 0,34,18)` | Oui
| V | 19 | ![skill 19](../gfx/skills/skill_19.png) | ![rifle_2](../gfx/weapons/rifle_2.png)           | M16                 | `( 34,18,34,18)` | Oui
| V | 22 | ![skill 22](../gfx/skills/skill_22.png) | ![rifle_3](../gfx/weapons/rifle_3.png)           | Thompson            | `( 34,36,34,18)` | Oui
| V | 23 | ![skill 23](../gfx/skills/skill_23.png) | ![rifle_4](../gfx/weapons/rifle_4.png)           | UMP                 | `( 34,54,34,18)` | Oui
| V | 21 | ![skill 21](../gfx/skills/skill_21.png) | ![rifle_5](../gfx/weapons/rifle_5.png)           | FAMAS               | `( 34,72,34,18)` | Oui
| V | 18 | ![skill 18](../gfx/skills/skill_18.png) | ![rifle_6](../gfx/weapons/rifle_6.png)           | Fusil d'assaut      | `( 34,90,34,18)` | Oui
| V | 14 | ![skill 14](../gfx/skills/skill_14.png) | ![shotgun_1](../gfx/weapons/shotgun_1.png)       | Fusil semi-auto     | `( 68, 0,34,18)` | Oui
| V | 13 | ![skill 13](../gfx/skills/skill_13.png) | ![shotgun_2](../gfx/weapons/shotgun_2.png)       | Fusil               | `( 68,18,34,18)` | Oui
| V | 16 | ![skill 16](../gfx/skills/skill_16.png) | ![shotgun_3](../gfx/weapons/shotgun_3.png)       | Fusil à pompe       | `( 68,36,34,18)` | Oui
| V | 15 | ![skill 15](../gfx/skills/skill_15.png) | ![shotgun_4](../gfx/weapons/shotgun_4.png)       | Fusil double        | `( 68,54,34,18)` | Oui
| V | 17 | ![skill 17](../gfx/skills/skill_17.png) | ![shotgun_5](../gfx/weapons/shotgun_5.png)       | Fusil à grenailles  | `( 68,72,34,18)` | Oui
| V | 32 | ![skill 32](../gfx/skills/skill_32.png) | ![sniper_1](../gfx/weapons/sniper_1.png)         | Sniper              | `(102, 0,50,18)` | Oui
| V | 33 | ![skill 33](../gfx/skills/skill_33.png) | ![sniper_2](../gfx/weapons/sniper_2.png)         | MOS-TECK            | `(102,18,50,18)` | Oui
| V | 34 | ![skill 34](../gfx/skills/skill_34.png) | ![sniper_3](../gfx/weapons/sniper_3.png)         | Lizardo Jungle      | `(102,36,50,18)` | Oui
| V | 35 | ![skill 35](../gfx/skills/skill_35.png) | ![sniper_4](../gfx/weapons/sniper_4.png)         | Epervier            | `(102,54,50,18)` | Oui
| V | 36 | ![skill 36](../gfx/skills/skill_36.png) | ![sniper_5](../gfx/weapons/sniper_5.png)         | CK-Magellan         | `(102,72,50,18)` | Oui
| V | 24 | ![skill 24](../gfx/skills/skill_24.png) | ![launcher_1](../gfx/weapons/launcher_1.png)     | Bazooka M1          | `(152, 0,50,18)` | Oui
| V | 25 | ![skill 25](../gfx/skills/skill_25.png) | ![launcher_2](../gfx/weapons/launcher_2.png)     | Lance-roquette      | `(152,18,50,18)` | Oui
| V | 26 | ![skill 26](../gfx/skills/skill_26.png) | ![launcher_3](../gfx/weapons/launcher_3.png)     | Bazooka M25         | `(152,36,50,18)` | Oui
| V | 27 | ![skill 27](../gfx/skills/skill_27.png) | ![launcher_4](../gfx/weapons/launcher_4.png)     | Tube Infernal       | `(152,54,50,18)` | Oui
| V | 28 | ![skill 28](../gfx/skills/skill_28.png) | ![machinegun_1](../gfx/weapons/machinegun_1.png) | Comanche auto       | `(202, 0,50,18)` | Oui
| V | 29 | ![skill 29](../gfx/skills/skill_29.png) | ![machinegun_2](../gfx/weapons/machinegun_2.png) | Mitrailleuse lourde | `(202,18,50,18)` | Oui
| V | 30 | ![skill 30](../gfx/skills/skill_30.png) | ![machinegun_3](../gfx/weapons/machinegun_3.png) | Sulfateuse          | `(202,36,50,18)` | Oui
| V | 32 | ![skill 31](../gfx/skills/skill_31.png) | ![machinegun_4](../gfx/weapons/machinegun_4.png) | Minigun             | `(202,54,50,18)` | Oui
| V | 37 | ![skill 37](../gfx/skills/skill_37.png) | ![knife_1](../gfx/weapons/knife_1.png)           | Couteau             | `(252, 0,34,18)` | Oui
| V | -- | --                                      | ![knife_2](../gfx/weapons/knife_2.png)           | *knife_2*           | `(252,18,34,18)` | *Non*
| V | 38 | ![skill 38](../gfx/skills/skill_38.png) | ![machinegun_5](../gfx/weapons/machinegun_5.png) | Mitrailleuse fixe   | `(286, 0,50,18)` | Oui
|  |  | ![skill 38](../gfx/skills/skill_.png) | ![machinegun_5](../gfx/weapons/machinegun_5.png)      | *tank lourd*        | `(286, 0,50,18)` | Oui
| V | 41 | ![skill 41](../gfx/skills/skill_41.png) | ![machinegun_5](../gfx/weapons/machinegun_5.png) | Mitrailleuse Hélico | `(286, 0,50,18)` | Oui
| V | 38 | ![skill 38](../gfx/skills/skill_38.png) | ![machinegun_6](../gfx/weapons/machinegun_6.png) | Canon               | `(286,18,50,18)` | Oui
|  |  | ![skill 38](../gfx/skills/skill_.png) | ![machinegun_6](../gfx/weapons/machinegun_6.png)      | *tank lourd*        | `(286,18,50,18)` | Oui

### ingame

Sprites disponibles au format SVG.

#### rifle

![](../../swf-exported/army/shapes/PNG/176.png)
![](../../swf-exported/army/shapes/PNG/177.png)
![](../../swf-exported/army/shapes/PNG/178.png)
![](../../swf-exported/army/shapes/PNG/179.png)

#### sniper

![](../../swf-exported/army/shapes/PNG/303.png)
![](../../swf-exported/army/shapes/PNG/304.png)
![](../../swf-exported/army/shapes/PNG/306.png)
![](../../swf-exported/army/shapes/PNG/307.png)
![](../../swf-exported/army/shapes/PNG/308.png)
![](../../swf-exported/army/shapes/PNG/310.png)
![](../../swf-exported/army/shapes/PNG/311.png)

#### knife

![](../../swf-exported/army/shapes/PNG/321.png)
![](../../swf-exported/army/shapes/PNG/573.png)

#### machinegun

![](../../swf-exported/army/shapes/PNG/372.png)
![](../../swf-exported/army/shapes/PNG/373.png)
![](../../swf-exported/army/shapes/PNG/375.png)
![](../../swf-exported/army/shapes/PNG/376.png)

#### shotgun

![](../../swf-exported/army/shapes/PNG/391.png)
![](../../swf-exported/army/shapes/PNG/392.png)
![](../../swf-exported/army/shapes/PNG/393.png)
![](../../swf-exported/army/shapes/PNG/394.png)
![](../../swf-exported/army/shapes/PNG/395.png)

#### handgun

![](../../swf-exported/army/shapes/PNG/540.png)
![](../../swf-exported/army/shapes/PNG/541.png)
![](../../swf-exported/army/shapes/PNG/542.png)

#### launcher

![](../../swf-exported/army/shapes/PNG/568.png)
![](../../swf-exported/army/shapes/PNG/569.png)
![](../../swf-exported/army/shapes/PNG/570.png)
![](../../swf-exported/army/shapes/PNG/571.png)
