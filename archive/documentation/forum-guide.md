# Guide

Publié en post-it sur le forum [source](http://twd.io/e/AhFM0g) 2013-04-30

## Minitroopers c'est quoi ?

Minitroopers est un jeu de stratégie où le but est d'avoir la plus forte armée.

### Comment aller sur minitroopers ?

Il faut aller sur les liens des factions

Les factions sont les armées créer par les créateurs du jeu.

### Liste des factions :

- <http://groin-celeste.minitroopers.fr> troopers violets
- <http://enchantee.minitroopers.fr> troopers verts
- <http://gecko-nevrose.minitroopers.fr> troopers jaunes
- <http://fol-ecume.minitroopers.fr> troopers bleus
- <http://sang-surs.minitroopers.fr> troopers rouges
- <http://soleil-rugissant.minitroopers.fr> troopers oranges

**ATTENTION**
**Annonce Importante : Vache Rose n'est pas une faction, c'est juste l'armée d'un joueur qui a oublié de mettre un mot de passe.**

### Les troopers :

Les troopers sont la base de votre armée. Ils sont petits, brutaux et ils sont sous vos ordres.
La première fois que vous choisirez votre trooper vous aurez plusieurs choix :

- Choix 1 : Deux armes.
- Choix 2 : Une arme et une capacité.

Ensuite, engagez les premiers combats.

Une victoire rapporte deux tokens et une défaite un seul.

Les cas possibles après trois combats:

- 3 victoires 6 tokens
- 2 vitoires et 1 défaite 5 tokens
- 1 victoire et 2 défaites 4 tokens
- 3 défaites 3 tokens

Donc, dans tous les cas possibles, vous pourrez augmenter votre trooper de niveau.

### Les changements de niveau

Pour passer du niveau 1 au 2 il faut 1 token (après le premier match, vous pourrez augmenter votre trooper.)

- Du niveau 2 au 3 : 5 tokens
- Du 3 au 4 : 15 tokens
- Du 4 au 5 : 32 tokens
- Du 5 au 6 : 55 tokens
- Du 6 au 7 : 88 tokens
- Du 7 au 8 : 129 tokens
- Du 8 au 9 : 181 tokens
- Du 9 au 10 : 243 tokens
- Du 10 au 11 : 316 tokens
- Etc ...

Au niveau 2, on peut choisir son arme préférée, puis sa Localisation Favorite.

### Quelques capacités spéciales :

**Malin :** Le trooper obtient un choix supplémentaire lors de la montée en niveau. Contre les effets de Appât, Grenade rose et Propagande. Divise par 2 les chances de sabotage par l'ennemi.

**Commandant :** Plus une bataille par jour (max 2).

Pour voir la liste des capacités spéciales et des armes, veuillez vous rendre > [par ici](#) <.

### La localisation favorite

La localisation favorite est la partie du corps de votre adversaire que votre trooper cherchera à toucher en premier.

### Les missions :

Les missions se débloquent pour 5 tokens. Il y a plusieurs missions.

**Les missions infiltration:** Vous attaquez une armée qui vous attaque sur le mode combat.

**La mission exterminer :** Vous devez tuer et exterminer les rats.

**La mission épique :** La mission épique est une mission qui se débloque quand on a 10 recrues. Votre armée et vos recrues affrontent des troopers.

Vous avez trois essais pour chaque mission. Une victoire vous rapporte 4 tokens. Une défaite 0. Une victoire stoppe aussi les compteurs.
Je m'explique
[Mon screen](#).

Des recrues pour le Raid

### Comment avoir des recrues ?

Pour avoir des recrues, c'est très simple : il suffit de donner le lien de votre armée en retirant le /hq. Veillez cependant à ne pas poster votre lien n'importe où.
Au début, vos recrues vous rapporteront des tokens. La première recrue vous en rapporte 100. Les 2°,3° et la 4° vous rapportent 50 tokens.

### Les armes préférées

Les armes préférées seront les armes que votre trooper aura en premier lors du combat.

### Les casques :

Lors du passage du niveau 5 au niveau 6, on ne vous proposera pas d'armes. Non, on vous proposera deux casques.

Le nom des casques plus leurs compétences

**Officer Com :** +1 point en communication à chaque niveau
**Saboteur :** +1 point de sabotage à chaque niveau
**Artificier :** Recharge les munitions de ses coéquipiers et augmente l'utilisation des grenades.
**Soldat :** Gagne 1 point de vie à chaque niveau
**Médecin :** Soigne vos équipiers. Insensible au poison
**Scout :** +1 en déploiment (max 20)
**Pilote :** Augmente de 25 % les chances de sortir un véhicule.
**Espion :** +50 en initiative (10 pts d'initiative supplémentaires pour chaque niveau). Apparaît dans le camp adverse.

### Les points comm.

Les points comm servent à faire des actions telles que :

**Verrouillé :** Un trooper est pris pour cible, et on ne peut pas le rater.

**Retraite :** Vos troopers se retirent et une vague fraîche apparaît.

**Commando :** Vos troopers gagnent de l'initiative.

**Propagande :** Un des troopers de votre adversaire passe de votre côté.

**Objectif solo :** Un de vos troopers gagne des points d'initiative.

### Les points sabotage

Les points sabotage permettent à votre Saboteur de saboter des armes adverses. Ces dernières sont alors inutilisables.

### Les Troopers en détail :

Pour les explications qui vont suivre, rendez-vous sur la page perso. d'un de vos Troopers. ( Au moins Niveau 5 )

Donc sur la partie centre vous avez toutes les Armes et Compétences de votre Trooper. Sur votre droite , il y a une multitude de réglages possibles :


**Arme préférée :** Si votre Trooper possède plus d'une arme , vous pourrez changer son arme préférée c'est-à-dire l'arme avec laquelle il apparaîtra lors du déploiement.

**Localisation Favorite :** Dans Minitroopers , il est possible de tuer un Trooper d'un coup avec une Blessure Fatale. Localisation Favorite détermine où le Trooper va essayer de faire une Blessure Fatale. ( Il y a des Compétences, comme Bourreau des Coeurs, qui augmentent les chances de Blessure Fatale, ici au coeur, donc je vous conseille de mettre le coeur en Localisation Favorite )

**Ciblage :** Rien de bien compliqué, Ciblage permet de régler sur quel type de Trooper votre Trooper va tirer. " Le Plus Proche " : Ton Trooper attaque l'ennemi le plus proche. " Le Plus Facile " : Il attaquera le Trooper qui a le moins de chances d'esquiver les tirs. " Le Plus Faible " : Celui qui a le moins de vie. Et enfin " Le Plus Menaçant " : Celui qui risque le plus de tuer votre Trooper ou un allié.

**Comportement :** Le comportement chasseur fera de votre Trooper un vrai fou qui veut tuer tout le monde ! Et en rentrant dans le tas ! Le comportement défensif permettra à votre Trooper de rester plus longtemps en vie ... Enfin en théorie ! Et pour finir le comportement Standard c'est entre les deux.

**Priorité :** Si votre Trooper peut tirer sur plusieurs ennemis, ce réglage permet de choisir sur quel Trooper il va tirer.

**Rééquiper :** Ce bouton s'obtient dès que votre Trooper possède au moins 4 equipements et permet de changer les équipements qu'il possède pour aller au combat.

Et oui, votre Trooper ne peut avoir que 3 Equipements en même temps !

Merci Flomimo et Don-Diego

Deathmania$5478052
