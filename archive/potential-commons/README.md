# Potential commons

Inventaires des assets, classes, etc. potentiellement en commun avec d'autres jeux MT.

## Muxxu / LaBrute

[Github repo WebGamesArchives : Muxxu/LaBrute](https://github.com/motion-twin/WebGamesArchives/tree/main/Muxxu/LaBrute)

### Fiche

 url | similarity
 ---|---
`http://labrute.muxxu.com/b/6922` | [`/t/0`](https://gitlab.com/eternaltwin/minitroopers/minitroopers-extra/-/blob/master/site-elements/sitemap.md#fiche)

![Cellule de Camille](screenshots/muxxu_labrute-Fiche.png "Screenshot")

### Skills


#### Tableau des skills

À la différence de minitroopers qui emploie JS et un spritesheet CSS, ce tableau dans LaBrute est représenté par un SWF.

[Github repo WebGamesArchives : Muxxu/LaBrute/fla/inventory](https://github.com/motion-twin/WebGamesArchives/tree/main/Muxxu/LaBrute/fla/inventory)

![Tableau des armes / skills](screenshots/muxxu_labrute-Skills.png  "Screenshot")

### Trivia

#### Le chien

![Max le chien](screenshots/muxxu_labrute-Chien.png "Screenshot")

### Comparatif des fichiers source.

(Ebauche arbitraire, sur basée sur les noms de fichiers)

`Muxxu/LaBrute/` | V | `Minitroopers`
---|:-:|---
`fla\client\src\Cs.hx`             |  | `Cs.as`
`fla\client\src\Fighter.hx`        | ? | `logic\Ghost.as`
`fla\client\src\Game.hx`           |  | `Game.as`
`fla\client\src\InterGlad.hx`      | ? | Inter*Gladiator* > `inter\*` (obfu ?)
`fla\client\src\Manager.hx`        |  | `mt\fx\Manager.as`
`fla\client\src\Part.hx`           |  | `mt\fx\Part.as`
`fla\client\src\Phys.hx`           | ? | 
`fla\client\src\Sprite.hx`         |  | `pix\Sprite.as`
`fla\client\src\State.hx`          | ? | 
`fla\client\src\part\Shade.hx`     |  | `gfx\TrooperShade.as`
`fla\client\src\st\Attack.hx`      | ? | 
`fla\client\src\st\Bomb.hx`        | ? | 
`fla\client\src\st\ComeIn.hx`      | ? | 
`fla\client\src\st\Death.hx`       | ? | 
`fla\client\src\st\Downpour.hx`    | ? | 
`fla\client\src\st\Eat.hx`         | ? | 
`fla\client\src\st\EndFight.hx`    | ? | 
`fla\client\src\st\Escape.hx`      | ? | 
`fla\client\src\st\Fx.hx`          |  | `mt\fx\Fx.as`
`fla\client\src\st\Grab.hx`        | ? | 
`fla\client\src\st\Hypno.hx`       | ? | 
`fla\client\src\st\Leave.hx`       | ? | 
`fla\client\src\st\Medecine.hx`    | ? | 
`fla\client\src\st\Move.hx`        | ? | 
`fla\client\src\st\Net.hx`         | ? | 
`fla\client\src\st\Poison.hx`      | ? | 
`fla\client\src\st\Status.hx`      |  | `Status.as`
`fla\client\src\st\Steal.hx`       | ? | 
`fla\client\src\st\ThrowAttack.hx` | ? | 
`fla\client\src\st\Trash.hx`       | ? | 
`fla\client\src\st\Weapon.hx`      |  | `Weapon.as`
`fla\inventory\Inventory.hx`       | ? | 
`fla\mini\code.as`                 | ? | 
`fla\perso\code.as`                | ? | 
`fla\tournoi\Tournoi.hx`           | ? | 
`src\com\Arena.hx`                 | ? | 
`src\com\Lang.hx`                  | ? | 
`src\com\Gladiator.hx`             | ? | `Trooper.as` `TrooperType.as`
`src\com\Data.hx`                  |  | `Data.as` <br>`mt\player\Data.as` <br>`CData.as` <br>`SData.as` <br>`VData.as` <br>`WData.as`


## DinoRPG

[Github repo WebGamesArchives : DinoRPG](https://github.com/motion-twin/WebGamesArchives/tree/main/DinoRPG)

### Comparatif des fichiers source.

(Ebauche arbitraire, sur basée sur les noms de fichiers)

`DinoRPG/` | V | `Minitroopers`
---|:-:|---
`src/data/Action.hx`              |  | `Action.as` <br>`mt/player/Action.as`
`gfx/fight/src/Cs.hx`             |  | `Cs.as`
`gfx/fight/src/Scene.hx`          |  | `Scene.as`
`gfx/fight/src/fx/gr/Charge.hx`   |  | `ac/Charge.as`
`gfx/fight/src/fx/gr/Heal.hx`     |  | `ac/Heal.as`
`gfx/fight/src/ac/SpawnToy.hx`    |  | `ac/Spawn.as` <br>`mt/fx/Spawn.as`
`src/data/Tools.hx`               |  | `format/swf/Tools.as`
`src/fx/gr/Hole.hx`               |  | `fx/BlackHole.as` <br>`gfx/MaskHole.as`
`gfx/fight/src/part/Bubbles.hx`   |  | `gfx/Bubble.as`
`gfx/fight/src/part/Shade.hx`     |  | `gfx/TrooperShade.as`
`gfx/fight/src/fx/Focus.hx`       |  | `gfx/fx/Focus.as`
`src/fight/CompatUnserializer.hx` |  | `haxe/Unserializer.as`
`src/fight/Fighter.hx`            |  | `logic/Ghost.as` <br>`Trooper.as` <br>`TrooperType.as`
`src/com/BattleData.hx`           |  | `logic/Battle.as`
`gfx/fight/src/Sprite.hx`         |  | `pix/Sprite.as`
`gfx/fight/src/Main.hx` <br>`gfx/dungeon/gen/Main.hx` |  | `Main.as`
`gfx/fight/src/ac/Goto.hx` <br>`gfx/fight/src/ac/GotoFighter.hx` |  | `ac/Goto.as` <br>`fx/Goto.as`
`gfx/dungeon/gen/Gameplay.hx` <br>`src/data/GameRewards.hx` <br>`src/data/GameVar.hx` |  |  `Game.as`
`src/data/Status.hx` <br>`gfx/fight/src/ac/NoStatus.hx` <br>`gfx/fight/src/ac/Status.hx` |  | `Status.as`
`gfx/fight/src/fx/Anim.hx` <br>`gfx/fight/src/fx/AttachAnim.hx` <br>`gfx/fight/src/fx/gr/Anim.hx` |  | `Anim.as` <br>`cache/Anim.as` <br>`pix/Anim.as`
`src/Skills.hx` <br>`src/data/Skill.hx` <br>`src/fight/SkillsImpl.hx` <br>`src/fight/skills/EnvSkill.hx` |  | `Skill.as`
`src/com/Data.hx` <br>`gfx/dungeon/gen/Data.hx` <br>`src/com/BattleData.hx` <br>`src/com/DungeonData.hx` <br>`src/com/GatherData.hx` <br>`src/com/MapData.hx` <br>`src/com/SneakData.hx` |  | `Data.as` <br>`mt/player/Data.as` <br>`CData.as` <br>`SData.as` <br>`VData.as` <br>`WData.as`