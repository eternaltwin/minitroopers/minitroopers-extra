// ==UserScript==
// @name           Minitroopers replay
// @namespace      minitroopers_replay
// @description    Sauvegarde vos combats minitroopers !
// @author         iow
// @include        *.minitroopers.fr/*
// @version        1.0b
// ==/UserScript==

var _mr_version = "1.0b";
var _mr_url = 'http://iow.ludiserv.com/mreplay/';
var _mr_url_split = window.location.href.split('/');

if(((_mr_url_split[_mr_url_split.length - 2] == 'view') || (_mr_url_split[_mr_url_split.length - 2] == 'fview')) && (_mr_url_split[_mr_url_split.length - 3] == 'b'))
{
	var _mr_is_save = false;
	var _mr_node_link = document.createElement('a');
	_mr_node_link.style.background = 'url(' + _mr_url + 'img/save.png)';
	_mr_node_link.style.display = 'block';
	_mr_node_link.style.width = '152px';
	_mr_node_link.style.height = '27px';
	_mr_node_link.style.backgroundPosition = '0px 0px';
	_mr_node_link.style.position = 'absolute';
	_mr_node_link.style.top = '27px';
	_mr_node_link.style.cursor = 'pointer';
	_mr_node_link.id = '_mr_link';
	_mr_node_link.addEventListener("click", _mr_send_data, true);
	_mr_node_link.addEventListener("mouseover", function () { this.style.backgroundPosition = '0px -27px'; }, true);
	_mr_node_link.addEventListener("mouseout", function () { this.style.backgroundPosition = '0px 0px'; }, true);

	var _mr_node_save = document.createElement('div');
	_mr_node_save.style.width = '152px';
	_mr_node_save.style.height = '27px';
	_mr_node_save.style.position = 'absolute';
	_mr_node_save.style.top = '-27px';
	_mr_node_save.style.left = '294px';
	_mr_node_save.style.overflow = 'hidden';
	_mr_node_save.appendChild(_mr_node_link);

	var _mr_nb = 26;
	var _mr_sliding = false;
	var _mr_node = document.getElementById('content').getElementsByTagName('div').item(0).getElementsByTagName('div').item(0);
	_mr_node.style.position = 'relative';
	_mr_node.appendChild(_mr_node_save);



	setTimeout(_mr_show_link, 1000);
}
else if(_mr_url_split[_mr_url_split.length - 1] == 'hq')
{
	GM_xmlhttpRequest({
					'method' : 'POST',
					'url' : _mr_url,
					'data' : "ajax=version",
					'headers' : {'Content-type' : 'application/x-www-form-urlencoded'},
					'onload' : _mr_version_back});
}

function _mr_show_link()
{
	_mr_sliding = true;
	document.getElementById('_mr_link').style.top = _mr_nb + 'px';
	if(_mr_nb > 0)
	{
		_mr_nb = parseInt(_mr_nb) - 2;
		setTimeout(_mr_show_link, 50);
	}
	else
		_mr_sliding = false;
}

function _mr_hide_link()
{
	_mr_sliding = true;
	document.getElementById('_mr_link').style.top = _mr_nb + 'px';
	if(_mr_nb < 27)
	{
		_mr_nb = parseInt(_mr_nb) + 2;
		setTimeout(_mr_hide_link, 50);
	}
	else
		_mr_sliding = false;
}



function _mr_send_data()
{
	if(!_mr_is_save)
	{
		if(_mr_sliding)
			setTimeout(_mr_send_data, 50);
		else
		{
			_mr_is_save = true;
			_mr_nb = 1;
			_mr_hide_link();
			data = 'ajax=add-replay&data=' + escape(document.getElementsByTagName('html').item(0).innerHTML) + '&url=' + window.location.href;
			GM_xmlhttpRequest({
				'method' : 'POST',
				'url' : _mr_url,
				'data' : data,
				'headers' : {'Content-type' : 'application/x-www-form-urlencoded'},
				'onload' : _mr_send_data_back,
				'onerror' : _mr_send_data_error});
		}
	}
}


function _mr_send_data_back(_mr_obj)
{
	if(_mr_obj['responseText'][0] == '0')
		_mr_send_data_error(_mr_obj);
	else if(_mr_obj['responseText'][0] == '2')
	{

		var _mr_node_link_deja_enregistre = document.createElement('a');
		_mr_node_link_deja_enregistre.style.background = 'url(' + _mr_url + 'img/deja-enregistre.png)';
		_mr_node_link_deja_enregistre.style.display = 'block';
		_mr_node_link_deja_enregistre.style.width = '198px';
		_mr_node_link_deja_enregistre.style.height = '27px';
		_mr_node_link_deja_enregistre.style.backgroundPosition = '0px 0px';
		_mr_node_link_deja_enregistre.style.position = 'absolute';
		_mr_node_link_deja_enregistre.style.top = '27px';
		_mr_node_link_deja_enregistre.style.cursor = 'pointer';
		_mr_node_link_deja_enregistre.id = '_mr_link_deja_enregistre';
		_mr_node_link_deja_enregistre.href = _mr_url + 'replay-' + _mr_obj['responseText'].substr(1) + '.html';
		_mr_node_link_deja_enregistre.target = '_blank';
		_mr_node_link_deja_enregistre.addEventListener("mouseover", function () { this.style.backgroundPosition = '0px -27px'; }, true);
		_mr_node_link_deja_enregistre.addEventListener("mouseout", function () { this.style.backgroundPosition = '0px 0px'; }, true);

		var _mr_node_deja_enregistre = document.createElement('div');
		_mr_node_deja_enregistre.style.width = '198px';
		_mr_node_deja_enregistre.style.height = '27px';
		_mr_node_deja_enregistre.style.position = 'absolute';
		_mr_node_deja_enregistre.style.top = '-27px';
		_mr_node_deja_enregistre.style.left = '276px';
		_mr_node_deja_enregistre.style.overflow = 'hidden';
		_mr_node_deja_enregistre.appendChild(_mr_node_link_deja_enregistre);

		_mr_node.appendChild(_mr_node_deja_enregistre);
		_mr_show_link_deja_enregistre_wait();
	}
	else if(_mr_obj['responseText'][0] == '1')
	{
		var _mr_node_link_replay = document.createElement('a');
		_mr_node_link_replay.style.background = 'url(' + _mr_url + 'img/lien-replay.png)';
		_mr_node_link_replay.style.display = 'block';
		_mr_node_link_replay.style.width = '127px';
		_mr_node_link_replay.style.height = '27px';
		_mr_node_link_replay.style.backgroundPosition = '0px 0px';
		_mr_node_link_replay.style.position = 'absolute';
		_mr_node_link_replay.style.top = '27px';
		_mr_node_link_replay.style.cursor = 'pointer';
		_mr_node_link_replay.id = '_mr_link_replay';
		_mr_node_link_replay.href = _mr_url + 'replay-' + _mr_obj['responseText'].substr(1) + '.html';
		_mr_node_link_replay.target = '_blank';
		_mr_node_link_replay.addEventListener("mouseover", function () { this.style.backgroundPosition = '0px -27px'; }, true);
		_mr_node_link_replay.addEventListener("mouseout", function () { this.style.backgroundPosition = '0px 0px'; }, true);

		var _mr_node_replay = document.createElement('div');
		_mr_node_replay.style.width = '127px';
		_mr_node_replay.style.height = '27px';
		_mr_node_replay.style.position = 'absolute';
		_mr_node_replay.style.top = '-27px';
		_mr_node_replay.style.left = '312px';
		_mr_node_replay.style.overflow = 'hidden';
		_mr_node_replay.appendChild(_mr_node_link_replay);

		_mr_node.appendChild(_mr_node_replay);
		_mr_show_link_replay_wait();
	}
}

function _mr_show_link_replay_wait()
{
	if(_mr_sliding)
		setTimeout(_mr_show_link_replay_wait, 100);
	else
	{
		_mr_nb = 26;
		_mr_show_link_replay();
	}
}

function _mr_show_link_replay()
{
	_mr_sliding = true;
	document.getElementById('_mr_link_replay').style.top = _mr_nb + 'px';
	if(_mr_nb > 0)
	{
		_mr_nb = parseInt(_mr_nb) - 2;
		setTimeout(_mr_show_link_replay, 50);
	}
	else
		_mr_sliding = false;
}

function _mr_show_link_deja_enregistre_wait()
{
	if(_mr_sliding)
		setTimeout(_mr_show_link_deja_enregistre_wait, 100);
	else
	{
		_mr_nb = 26;
		_mr_show_link_deja_enregistre();
	}
}

function _mr_show_link_deja_enregistre()
{
	_mr_sliding = true;
	document.getElementById('_mr_link_deja_enregistre').style.top = _mr_nb + 'px';
	if(_mr_nb > 0)
	{
		_mr_nb = parseInt(_mr_nb) - 2;
		setTimeout(_mr_show_link_deja_enregistre, 50);
	}
	else
		_mr_sliding = false;
}


function _mr_send_data_error(_mr_obj)
{

	var _mr_node_link_error = document.createElement('div');
	_mr_node_link_error.style.background = 'url(' + _mr_url + 'img/erreur.png)';
	_mr_node_link_error.style.width = '127px';
	_mr_node_link_error.style.height = '27px';
	_mr_node_link_error.style.position = 'absolute';
	_mr_node_link_error.style.top = '27px';
	_mr_node_link_error.id = '_mr_link_error';

	var _mr_node_error = document.createElement('div');
	_mr_node_error.style.width = '127px';
	_mr_node_error.style.height = '27px';
	_mr_node_error.style.position = 'absolute';
	_mr_node_error.style.top = '-27px';
	_mr_node_error.style.left = '312px';
	_mr_node_error.style.overflow = 'hidden';
	_mr_node_error.appendChild(_mr_node_link_error);

	_mr_node.appendChild(_mr_node_error);
	_mr_show_link_error_wait();
}

function _mr_show_link_error_wait()
{
	if(_mr_sliding)
		setTimeout(_mr_show_link_error_wait, 100);
	else
	{
		_mr_nb = 26;
		_mr_show_link_error();
	}
}

function _mr_show_link_error()
{
	_mr_sliding = true;
	document.getElementById('_mr_link_error').style.top = _mr_nb + 'px';
	if(_mr_nb > 0)
	{
		_mr_nb = parseInt(_mr_nb) - 2;
		setTimeout(_mr_show_link_error, 50);
	}
	else
	{
		_mr_nb = 1;
		setTimeout(_mr_hide_link_error, 3000);
	}
}

function _mr_hide_link_error()
{
	document.getElementById('_mr_link_error').style.top = _mr_nb + 'px';
	if(_mr_nb < 27)
	{
		_mr_nb = parseInt(_mr_nb) + 2;
		setTimeout(_mr_hide_link_error, 50);
	}
	else
	{
		_mr_node.removeChild(document.getElementById('_mr_link_error').parentNode);
		_mr_nb = 26;
		_mr_is_save = false;
		_mr_show_link();
	}
}

function _mr_version_back(_mr_obj)
{
	if(_mr_trim(_mr_obj['responseText']) != _mr_version)
	{
		if(confirm('Une nouvelle version du script " Minitroopers - replay " est disponible. Installer maintenant ?'))
			window.location.href = _mr_url + 'javascript.php?script=mreplay.user.js';
	}
}

function _mr_trim(_mr_string)
{
	return(_mr_string.replace(/^\s+/g,'').replace(/\s+$/g,''));
}

/*
     FILE ARCHIVED ON 12:59:59 Jun 03, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 11:47:05 Jul 26, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 111.487
  exclusion.robots: 0.107
  exclusion.robots.policy: 0.087
  RedisCDXSource: 0.646
  esindex: 0.007
  LoadShardBlock: 95.607 (3)
  PetaboxLoader3.datanode: 186.512 (5)
  CDXLines.iter: 12.771 (3)
  PetaboxLoader3.resolve: 202.048 (3)
  load_resource: 304.073 (2)
*/